class_name IdlePlan extends Planner

var idle_behaviour : Behaviour

func _setup_behaviours():
	idle_behaviour = IdleBehaviour.new(self, "IdleBehaviour")
	#idle_behaviour2 = IdleBehaviour2.new(self, "IdleBehaviour2")

	# default behav
	switch_behaviour_to(idle_behaviour)

func _choose_behaviour():
	switch_behaviour_to(idle_behaviour)
	#if behaviour != agent.territory_plan:
		#if agent.energy < 10:
			#switch_behaviour_to(idle_behaviour)
		#else:
		#	switch_behaviour_to(other_idle_behaviour)
