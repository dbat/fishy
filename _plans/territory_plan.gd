class_name TerritoryPlan extends Planner

var territory_patrol_behaviour : Behaviour


func _setup_behaviours():
	territory_patrol_behaviour = TerritoryPatrolVaguelyBehaviour.new(self, "TerritoryPatrolVaguelyBehaviour")

	# default behaviour
	switch_behaviour_to(territory_patrol_behaviour)

func _choose_behaviour():
	switch_behaviour_to(territory_patrol_behaviour)
