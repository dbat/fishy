@tool
class_name BasePlan
extends Resource

## The metadata all goes into this method:
static func rw_metadata():
	return {
	&"category" : &"AI", # Optional
	&"partial_save_name" : &"plan", # influence the resource filename
	&"noinstance" : false # true would exclude this resource from the graph.
	}


enum {STATUS_ENDING, STATUS_ENDED, STATUS_INIT, STATUS_DURING} # status
var status : int = STATUS_ENDED
var plan_step: int = 0

var agent : RigidBody3D
var name : StringName

func _is_valid() -> bool: return false
func _priority(): pass


# The major categories of stats that the class 'involves'
# Used to decide how to inc/dev the internal state of the agent.
func _involves_energy() -> bool: return false
func _involves_security() -> bool: return false


func _run_process(delta: float) -> void: pass
func _run_physics_process(delta: float) -> void: pass
func _run_integrate_forces(state: PhysicsDirectBodyState3D) -> void : pass


#func end_current_behaviour():
	#status_to_ending()
	#behaviour._end()


#func switch_step_to(s:int):
	## make sure the current behav ends properly:
	##if behaviour: behaviour._end()
	##behaviour = s
	#status_to_init()
	##behaviour.status_to_init() TODO


func _begin(): pass
func _end(): pass

func plan_running() -> bool : return status > STATUS_ENDED

func status_is_init() -> bool : return status == STATUS_INIT
func status_is_during() -> bool : return status == STATUS_DURING
func status_is_ending() -> bool : return status == STATUS_ENDING
func status_is_ended() -> bool : return status == STATUS_ENDED
func none() -> bool : return status == STATUS_ENDED

func status_new_plan_starting() -> bool : return status > STATUS_ENDED

func status_to_init(): status = STATUS_INIT

func status_to_during(): status = STATUS_DURING
func status_to_ending(): status = STATUS_ENDING
func status_to_ended(): status = STATUS_ENDED
