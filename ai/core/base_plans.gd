@tool
class_name BasePlans
extends Resource

## The metadata all goes into this method:
static func rw_metadata():
	return {
	#&"display_class_name_as" : &"Plan Resource", # What appears on the node itself
	&"category" : &"GOAP AI", # Optional
	&"partial_save_name" : &"plans", # influence the resource filename
	&"noinstance" : false # true would exclude this resource from the graph.
	}

@export_group("Goals to stay..", "stay_")
@export var stay_alive : Array[BasePlan]
@export var stay_secure : Array[BasePlan]
@export var stay_fed : Array[BasePlan]
@export var stay_rested : Array[BasePlan]


var _all_plans_dict : Dictionary # key "agent" = Array[plan, plan..]
var _default_plan_resource := DefaultPlan.new() # Only one; ever.


## Call this from ready() of your agent.
## Using the agent as a key, populate a cache of unique
## (duplicated) plan objects for it.
## It find the export array fields by introspection.
func make_unique_plans_for(agent:RigidBody3D):
	# Seeking all the BasePlan array props in the plans_collection_resource
	# { "name": "stay_rested", "class_name": &"", "type": 28, "hint": 23, "hint_string": "24/17:BasePlan", "usage": 4102 }
	# 28 means Array. hint_string has BasePlan.
	var all_plan_arrays := get_property_list().filter(
			func(e):	return e.type == 28 and "BasePlan" in e.hint_string)

	# Now get the actual resources out of those array props
	var base_plan_array := all_plan_arrays.map(func(e): return get(e.name))

	# Put the default plan in idx 0
	_all_plans_dict[agent] = [_default_plan_resource]

	# Append all the other plans after that (flat array)
	for prop_array:Array in base_plan_array:
		for p:BasePlan in prop_array:
			var dup := p.duplicate()
			dup.agent = agent
			_all_plans_dict[agent].append(dup)


func get_plans_for(agent) -> Array[BasePlan]:
	# Weird hoops to return typed array.
	# I could not simply: return _all_plans_dict[agent]
	# It was complaining about the array types...
	var tmp : Array[BasePlan]
	for p:BasePlan in _all_plans_dict[agent]:
		tmp.append(p)
	return tmp
