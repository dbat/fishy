extends Node2D
var count : int = 6400000

var div_start : int = 0
var div_end : int = 0
var mul_start : int = 0
var mul_end : int = 0

func _ready() -> void:
	test_floats()
	test_ints()

func test_floats():
	var calc := 0.0

	div_start = Time.get_ticks_usec()
	for i in range(0,count):
		#calc = 1 / 2.0
		calc = 4.0 / 2.0
	div_end = Time.get_ticks_usec()

	mul_start = Time.get_ticks_usec()
	for i in range(0,count):
		#calc = 1 * 0.5
		calc = 8.0 * 0.5
	mul_end = Time.get_ticks_usec()

	report("float")
	get_tree().quit()

func report(what):
	var div = div_end - div_start
	var mul = mul_end - mul_start
	var diff = mul - div
	print("%s %d div, %d mul, diff %d" % [what, div, mul, diff])

func test_ints():
	var calc : int = 0

	div_start = Time.get_ticks_usec()
	for i:int in range(0, count):
		calc = 8 / 2
	div_end = Time.get_ticks_usec()

	mul_start = Time.get_ticks_usec()
	for i in range(0,count):
		calc = 2 * 2
	mul_end = Time.get_ticks_usec()

	report("int")
	get_tree().quit()
