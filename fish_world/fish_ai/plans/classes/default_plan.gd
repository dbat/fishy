@tool
class_name DefaultPlan
extends BasePlan


func _init() -> void:
	name = &"Default Plan"


func _is_valid() -> bool:
	return true


func _priority():
	return 1
