@tool
class_name AvoidHarmPlan
extends BasePlan

## The metadata all goes into this method:
#static func rw_metadata():
	#var d = super()
	#d[&"display_class_name_as"] = "Avoid Harm Plan"
	#return d

func _init() -> void:
	name = &"Avoid Harm"
