@tool
class_name DeathPlan
extends BasePlan

func _init() -> void:
	name = &"Death Plan"


### ---- The actual behaviour ---- ###

func _begin():
	status_to_init()

func _run_physics_process(delta):
	if status_is_init():
		agent.gravity_scale = 1.0 # dead fish sinks
		_end()

func _end():
	status_to_ended()
