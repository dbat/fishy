@tool
class_name RestHangDownPlan
extends BasePlan

## The metadata all goes into this method:
#static func rw_metadata():
	#var d = super()
	#d[&"display_class_name_as"] = "Rest Plan (hang)"
	#return d


func _init() -> void:
	name = &"Rest Plan (hang)"


func _involves_energy() -> bool:
	return true # resting does 'involve' energy


func _is_valid() -> bool:
	return agent.energy < 95 # how do i best vary this const per fish?


func _priority():
	# also include hunger maybe
	#  hunger < agent.HUNGER_CRITICAL
	return 1 if agent.energy > 95 else 2


### ---- The actual behaviour ---- ###

func _begin():
	status_to_init()

#enum {SLOWDOWN, STOPPED}
#var substate := SLOWDOWN

func _run_physics_process(delta):
	agent.linear_damp = 20
	#agent.angular_damp = 60

	var target : Vector3
	if status_is_init():
		target = agent.get_downish_v3(6) # finds downish
		status_to_during()

	# hold position and drop head down somewhat
	if status_is_during():
		# so we can slow-down (by damping) and then stop.
		if agent.linear_velocity.is_zero_approx():
			agent.lock_linear_axis_xyz(true) #NB

		agent.energy = clamp(agent.energy + 0.5, 0, 100)

		agent.look_at_target(target, delta)


	if status_is_ending():
		_end()


func _end():
	agent.lock_linear_axis_xyz(false) #NB
	status_to_ended()
