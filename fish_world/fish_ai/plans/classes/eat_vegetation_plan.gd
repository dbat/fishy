@tool
class_name EatVegetationPlan
extends BasePlan

## The metadata all goes into this method:
#static func rw_metadata():
	#var d = super()
	#d[&"display_class_name_as"] = "Eat Veggies Plan"
	#return d

@export var veg_kind : StringName = &"kelp"

func _init() -> void:
	name = &"Eat Veggies"

func _is_valid():
	pass
	# if near to kelp -> true
