@tool
class_name EatMorselPlan
extends BasePlan

## The metadata all goes into this method:
#static func rw_metadata():
	#var d = super()
	#d[&"display_class_name_as"] = "Eat Morsel Plan"
	#return d

@export var morsel_size : float = 0.1

func _init() -> void:
	name = &"Eat Morsel"


func _is_valid():
	pass
	# if near to a morsel -> true
	# or maybe if far from any other food, default to a morsel.
