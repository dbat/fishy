@tool
class_name RestCirclePlan
extends BasePlan

## The metadata all goes into this method:
#static func rw_metadata():
	#var d = super()
	#d[&"display_class_name_as"] = "Rest Plan (circle)"
	#return d

enum {RESTING}

#@export_enum("Hang_down", "Circle", "Hide") var rest_type: int


func _init() -> void:
	name = &"Rest Plan (circle)"
	plan_step = RESTING


func _involves_energy() -> bool:
	return true # resting does 'involve' energy


### ---- The actual behaviour ---- ###

func _begin():
	status_to_init()


func _run_physics_process(delta):
	agent.linear_damp = 60
	agent.angular_damp = 60

	var target : Vector3
	if status_is_init():
		status_to_during()

	if status_is_during():
		agent.energy += 0.1
		if agent.energy > 100:
			status_to_ending()

	if status_is_ending():
		_end()


func _end():
	status_to_ended()
