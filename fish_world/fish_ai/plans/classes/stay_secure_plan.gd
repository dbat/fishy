@tool
class_name StaySecurePlan
extends BasePlan

## The metadata all goes into this method:
static func rw_metadata():
	var d = super()
	d[&"display_class_name_as"] = "Stay Secure (Patrol) Plan"
	return d

enum {PATROLLING}


func _init() -> void:
	name = &"Patrolling"
	plan_step = PATROLLING


func _involves_security() -> bool:
	return true


func _is_valid() -> bool:
	return agent.security < 90


func _priority():
	return 1 if agent.security > 90 else 2

#enum {SPEEDUP, SPEEDMAXED}
#var substate := SPEEDUP

func _begin():
	status_to_init()


func _run_physics_process(delta):
	#print("territory status:", plan.status)
	agent.linear_damp = 5
	agent.angular_damp = 10

	if status_is_init():
		# A small boost from prev state
		agent.swim(delta)
		status_to_during()

	# head vaguely towards some target and arb about
	if status_is_during():
		#agent.security += 0.5
		agent.security = clamp(agent.security + 0.8, 0, 100)
		#print(agent.security)
		#if agent.security > 100:
			#status_to_ending()
		agent.vaguely_head_towards(agent.followme.position, delta)
		#agent.swim(delta)

	if status_is_ending():
		_end()


func _end():
	#print("patrol ended") # but what now?
	status_to_ended()
