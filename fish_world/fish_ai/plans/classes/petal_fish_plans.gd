@tool
class_name PetalFishPlans
extends BasePlans

## The metadata all goes into this method:
static func rw_metadata():
	var d = super()
	d[&"display_class_name_as"] = "Petal Fish Plans"
	return d

@export var name:String = "Petal Fish Plan Collection"
@export var preview_this : Mesh
