@tool
class_name DecayPlan
extends BasePlan

func _init() -> void:
	name = &"Decay Plan"

### ---- The actual behaviour ---- ###

func _begin():
	status_to_init()

func _run_physics_process(delta):
	if status_is_init():
		agent.gravity_scale = -0.1 # decaying fish float ?
		_end()

func _end():
	status_to_ended()
