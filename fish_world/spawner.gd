extends Area3D

@export var number:int = 10

var petal_fish: PackedScene = preload("res://fish_world/petalfish.tscn")
var plans := preload("res://fish_world/fish_ai/plans/resources/PetalFishPlans_plans_de10d663b3.tres")


func _ready() -> void:
	print("spaw zone ready")


func spawn() -> void:
	for n in range(0,number):
		var fish : PetalFish = petal_fish.instantiate()
		fish.plans_collection_resource = plans
		fish.position = position
		get_parent_node_3d().add_child(fish)
