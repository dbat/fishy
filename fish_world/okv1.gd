class_name Fishyv1 extends RigidBody3D

## This works only because the origin of the fish is NOT
## THE SAME as the centre_of_mass. Also the position of
## the various meshes and colliders in the fish are
## offset so that the origin is closer to the head.
## These small things make a HUGE difference.

@export var speed : float = 1

@export var ALARM := false
@export var hunger := 100.0
@export var security := 10.0
@export var energy := 100.0


var q:PhysicsShapeQueryParameters3D
var nf:Array
var intent := Intent.new(self)


func randomize_my_stats():
	hunger = randf_range(0, hunger)
	security = randf_range(0, security)
	energy = randf_range(0, energy)


func _ready() -> void:
	angular_damp = 60
	linear_damp = 60
	center_of_mass = %com.position

	#randomize_my_stats()

	# Start fish off chilling
	intent.switch_to_rest()

	# Code for the fish's 'eyes'
	q = PhysicsShapeQueryParameters3D.new()
	#q.exclude.append(self.get_rid()) # is not working...
	q.exclude = [self.get_rid()] # wierd, but this works!
	q.collide_with_bodies = true
	q.collision_mask = 2
	# NOTE:
	# %eyes is a CollisionShape3D and it is DISABLED!
	# This means it does NOT participate in collisions.
	# I can use it to scan for intersections with the
	# PhysicsServer stuff! Yay!
	%eyes.disabled = true
	q.shape = %eyes.shape
	q.margin = 0#.02





### ------------ PHYSICS ------------ ###

func _process(delta: float) -> void:
	const fskip1 := 20
	if Engine.get_physics_frames() % fskip1 == 0:
		decide_what_to_do()


func _physics_process(delta: float) -> void:
	move_target(delta)
	if intent.status_new_intent_starting():
		#linear_damp = 20
		#angular_damp = 4
		intent.call_task_func(delta)
		#print(intent)


func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	const fskip1 := 4
	var frames := Engine.get_physics_frames()
	if frames % fskip1 == 0:
		## The only way I could find to kind of keep the fin up
		# TODO ERROR: The target vector and up vector can't be parallel to each other.
		var up = -state.total_gravity
		# The up vector can't be zero in looking_at
		if up.is_zero_approx():
			up = position - %fishspace.position

		#print("up:", up, " vs bz:", -basis.z)
		state.transform.basis = basis.slerp(
			state.transform.basis.looking_at(-basis.z, up),
			state.step * fskip1
		).orthonormalized()

		#if head_down and once:
			##rotate_x(PI/90)
			##return
			#var tween = get_tree().create_tween().set_loops(90)
			#print("head down tween starts")
			#tween.tween_callback(rotate_x.bind(deg_to_rad(1))).set_delay(state.step)
			#print("head down tween call is done")
			#once = false
			#head_down = false






### ------------ INTENTIONS ------------ ###

func change_stats():
	#print( intent.resting())
	energy -= int(not intent.resting())
	security -= int(not intent.patrolling())


func decide_what_to_do():
	change_stats()
	p()
	if ALARM:
		intent.switch_to_flee(); return
	if security < 90:
		intent.switch_to_patrol()

	# right now it's patrol or rest
	if not intent.patrolling() or energy < 50:
		intent.switch_to_rest()





### ------------ TASKS ------------ ###

func rest(delta):
	linear_damp = 60
	angular_damp = 60
	var target : Vector3
	if intent.status_is_init():
		target = get_downish_v3()
		gravity_scale = 0.0
		axis_lock_linear_x = true
		axis_lock_linear_y = true
		axis_lock_linear_z = true
		intent.status_to_during()

	# reduce gravity until still
	# hold position and drop head down somewhat
	if intent.status_is_during():
		energy += 0.1
		if energy > 100:
			intent.status_to_ending()
		look_at_target(target, delta)

	if intent.status_is_ending():
		# how bad is it to alter outside of integrate_forces?
		#var tween = get_tree().create_tween().set_loops(90)
		#tween.tween_callback(rotate.bind(basis.x, deg_to_rad(+1))).set_delay(delta)
		#sleeping = false
		axis_lock_linear_x = false
		axis_lock_linear_y = false
		axis_lock_linear_z = false
		intent.status_to_ended()


func patrol(delta):
	linear_damp = 20
	angular_damp = 4
	if intent.status_is_init():
		gravity_scale = 1.0
		intent.status_to_during()

	# head vaguely towards some target and arb about
	if intent.status_is_during():
		security += 0.1
		if security > 100:
			intent.status_to_ending()
		vaguely_head_towards(%follow.position, delta)
		swim(delta)

	if intent.status_is_ending():
		pass

func find_food(delta): pass
func flee(delta): pass



#find_food:
# mark 'home'
# remember last food pos, if any
# angle down
# sweep area for hits
#  choose best hit
#  swim toward it
#  eat
#  return to 'home' pos
# if none:
#  swim (down or towards memory)
# if no hits for <time> randomize direction



### ------------ MOVEMENTS / ANIMATIONS ------------ ###

func swim(delta):
	var fwd = -basis.z * delta * speed
	apply_central_impulse(fwd)


func closely_follow_target(target:Vector3):
	var F : Vector3 = target - position
	apply_force(F)


func vaguely_head_towards(target:Vector3, delta):
	var F : Vector3
	var d := position.dot(target) + 1 # 0 away, 1 90, 2 towards
	var turn_speed := speed * 2
	F = position.move_toward(target, delta * d * turn_speed) - position
	apply_force(F)


func look_at_target(target:Vector3, delta):
	var F : Vector3 = target - position
	apply_force(F * delta)







### ------------ ASSORTED FUNCS ------------ ###

func get_downish_v3(delta:=2.0) -> Vector3:
	var a := -basis.z
	var g := get_gravity()
	var out := a.move_toward(g, delta)
	#DebugDraw3D.draw_arrow_ray(position,out,1,Color.TURQUOISE,0.01,false,60)
	return out


var scan_num := 8
func _get_nearby_fish(state:PhysicsDirectBodyState3D):
	#q.transform = %senses/ahead.global_transform
	q.transform = %eyes.global_transform
	var ss := state.get_space_state()
	nf = ss.intersect_shape(q, scan_num)
	#print(nf)
	if nf:
		var seen := nf.size()
		#print(nf.size())
		#print( self, " hits ", nf[0].collider)
		#if scan_num < f.size(): scan_num = f.size()
		if seen < scan_num:
			scan_num = wrapi(seen, 2, 8)


var w:float = 0
@onready var op : Vector3 = %follow.position
func move_target(delta):
	## Makes the target go up and down
	var norm : Vector3 = %follow.basis.y#.normalized()
	w += delta/1
	var v = (0.2 * sin(w)) * norm
	v.x = 0.2 * sin(w)
	#print(sin(w))
	%follow.position = op + v


func p():
	var t
	match intent.task:
		0 : t = "RESTING"
		1 : t = "PATROLLING"
		_ : t = "?"
	%t.text = "%s\nenergy:%0.0f\nsecurity:%0.0f" % [t, energy, security]






### ------------ INTENT STATE MACHINE ------------ ###

class Intent extends RefCounted:
	enum {REST, PATROL, FINDFOOD, FLEE} # task
	#done this way so I do things like:
	#         status > STATUS_ENDED--->
	enum {STATUS_ENDING, STATUS_ENDED, STATUS_INIT, STATUS_DURING} # status
	var task:int = REST
	var status:int = STATUS_ENDED
	var funcmap := {}

	func _init(fish) -> void:
		funcmap = {
			REST : fish.rest,
			PATROL : fish.patrol,
			FINDFOOD : fish.find_food,
			FLEE : fish.flee
		}

	func _get_task_func() -> Callable:
		return funcmap[task]

	func call_task_func(delta):
		var _task := _get_task_func()
		print("Calling:", _task.get_method())
		if _task.is_valid():
			_task.call(delta)

	func _switch(_task) -> void:
		# tell the current task it's over
		status = status_is_ending()
		call_task_func(0) # call it!
		# now set the new task
		task = _task
		status = STATUS_INIT

	func switch_to_rest(): _switch(REST)
	func switch_to_patrol(): _switch(PATROL)
	func switch_to_find_food(): _switch(FINDFOOD)
	func switch_to_flee(): _switch(FLEE)

	func resting() -> bool:	return task == REST and status > STATUS_ENDED
	func patrolling() -> bool:	return task == PATROL and status > STATUS_ENDED
	func hunting() -> bool: return task == FINDFOOD and status > STATUS_ENDED
	func fleeing() -> bool: return task == FLEE and status > STATUS_ENDED

	func status_is_init() -> bool : return status == STATUS_INIT
	func status_is_during() -> bool : return status == STATUS_DURING
	func status_is_ending() -> bool : return status == STATUS_ENDING
	func status_is_ended() -> bool : return status == STATUS_ENDED

	func status_new_intent_starting() -> bool : return status > STATUS_ENDED

	func status_to_during(): status = STATUS_DURING
	func status_to_ending(): status = STATUS_ENDING
	func status_to_ended(): status = STATUS_ENDED

	func _to_string() -> String:
		return "Intent: %s task: %s status:%s" % [self.get_instance_id(), task, status]
