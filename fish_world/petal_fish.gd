class_name PetalFish extends RigidBodyFish3D


var followme


# I need a more convenient way to reach the plans, hence:
#var my_plans : PetalFishPlans :
	#get:
		#return plans_collection as PetalFishPlans

var op : Vector3

func _ready() -> void:
	print("petalfish ", name, " ready")
	#print(my_plans)
	super()
	agent_mesh = $petalfish/petalfish
	agent_material = agent_mesh.get_active_material(0)

	followme = get_node(^"../follow")
	op = followme.position #%follow.position


func _change_stats():
	energy -= 0.2 * int(not plan._involves_energy())
	security -= 0.8 * int(not plan._involves_security())


func _physics_process(delta: float) -> void:
	super(delta)
	move_target(delta)


var w:float = 0

func move_target(delta):
	## Makes the target go up and down
	var norm : Vector3 = followme.basis.y#.normalized()
	w += delta
	var v = Vector3.ZERO#op#(2 * sin(w)) * norm
	v.x = 6 * sin(w)
	v.z = 6 * cos(w)
	#v.y = op.y * norm
	#print(sin(w))
	#%follow.position = op + (0.5*v)
	followme.position = op + v #0.5*v
