extends Node2D

enum ItemsEnum {
	 ITEM_ONE,
	 ITEM_TWO,
}
const items_data: Dictionary = {
	 ItemsEnum.ITEM_ONE: {
		  "value": 1,
		  "title": "Title one",
		  "tooltip": "Tooltip one",
	 },
	 ItemsEnum.ITEM_TWO: {
		  "value": 2,
		  "title": "Title two",
		  "tooltip": "Tooltip two",
	 },
}

func _ready():
	for id:int in items_data:
		self.do_stuff_with_items_data(
				id,
				items_data[id].value as int,
				items_data[id].title,
				items_data[id].tooltip,
		  )

func do_stuff_with_items_data(id: int, value: int, title: String, tooltip: String):
	pass # etc
