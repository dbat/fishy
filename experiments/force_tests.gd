extends RigidBody3D


#@freakazoid@retro.social
#I'm guessing you are applying a force vector at the origin to move the fish around, then letting inertia drive the orientation of the fish? That'll work until you try to simulate any other forces, at which point the fish will become unstable with its center of mass too far back.
#
#Thrust and steering are two separate forces. Thrust would always apply through the central axis, straight forward. That doesn't apply any torque. Steering is a sideways force applied somewhere near the tail fin. The torque is the steering force multiplied by the distance from the center of mass. When you represent torque as a vector, it is always parallel to the turning axis, i.e. vertical for steering. The direction depends on the physics system, which will use either the right hand rule or the left hand rule - use that hand and curl it so the fingers are pointing in the direction of the force you're applying. Thumb will point in the direction of the torque vector.
#
#That's just one axis, of course. Moment of inertia is a vector that represents the resistance to turning on all three axes, following the same rule as converting a sideways force to a torque. Each component is in units of mass x distance, which is exactly equivalent to having that mass at the end of a stick of that length. The important thing to remember is that the axis that a given component of the moment of inertia lies on is at a right angle to the direction the stick would point, determined by the handedness of the physics system.
#
#For now I'd recommend just dealing with the vertical axis (yaw/steering). Set the other two components of the moment of inertia really high. Then you can just compute thrust and turning force.


var testnumber
var impact_point
var F
var go:=false

func _ready() -> void:
	center_of_mass = %com.position

	# default test
	F = basis.x
	impact_point = F

	testnumber = 4

	match testnumber:
		1: pass
		2: # testing the 'offset' param
			impact_point = position + Vector3.FORWARD
			print(impact_point) #(0, -0.002722, -1)
			impact_point = to_global(position + Vector3.FORWARD) # use this one
			print(impact_point) #(0, -0.005444, -1)
			# So the impact_point nums are diff. docs say to use global.
			impact_point = to_global(position + Vector3.FORWARD + Vector3.FORWARD)
		3: # Torque tests
			F = 1 * F
		4: # Torque and damping tests
			F = 1 * F
			angular_damp = 10

	%impact_point.position = impact_point


func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventKey and not go:
		if event.keycode == KEY_SPACE:
			go = true
			match testnumber:
				1 : apply_impulse(F)
				2 : apply_impulse(F, impact_point)
				3 : apply_torque_impulse(F)
				4 : apply_torque_impulse(F)


func _physics_process(delta: float) -> void:
	if impact_point:
		DebugDraw3D.draw_arrow_ray(impact_point-F, F, 1.0, Color.AQUA, 0.1)
