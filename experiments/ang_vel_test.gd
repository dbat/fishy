extends RigidBody3D

## Findings
##
## I think there is no 1 damp setting that can control a torque
## such that it stops after n radians.
## damp = 0 means 1-(0/60) wich is 1 which means keep going
## damp = 60 means 0 which means full stop
##  FAIL:
##  I think I have to slide from 0 to 60 within one second
##  to get a PI radians per second torque to complete
##  NOPE - that way lies madness
## OKAY:
##  What worked was to let it be 0, count to 60 frames and
##  then set it to 60, to stop it:

func _ready() -> void:
	pass

var once:bool = true
func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		if once:
			var T = basis.x * PI
			print("Started torque on basis.x:", T )
			angular_damp = 0
			apply_torque_impulse(T)
			once = false
			dodamp = true

var dodamp:bool =false
var tick:int = 0
func _physics_process(delta: float) -> void:
	#print("  damp calc:", 1-(angular_damp/Engine.physics_ticks_per_second))
	if dodamp:
		tick += 1
		if tick % 60 == 0:
			angular_damp = 60
			print(angular_damp)
