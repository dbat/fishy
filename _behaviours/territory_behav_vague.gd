class_name TerritoryPatrolVaguelyBehaviour extends Behaviour


func run_physics_process(delta):
	#print("territory status:", plan.status)
	agent.linear_damp = 5
	agent.angular_damp = 10

	if plan.status_is_init():
		agent.gravity_scale = 1.0
		plan.status_to_during()

	# head vaguely towards some target and arb about
	if plan.status_is_during():
		agent.security += 0.001
		#print(agent.security)
		if agent.security > 100:
			plan.status_to_ending()
		agent.vaguely_head_towards(agent.followme.position, delta)
		#agent.swim(delta)

	if plan.status_is_ending():
		_end()


func _end():
	plan.status_to_ended()
