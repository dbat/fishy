class_name IdleBehaviour extends Behaviour

var _old_gravity_scale := 0.0

func run_physics_process(delta):
	agent.linear_damp = 60
	agent.angular_damp = 60
	#print("idle status:", plan.status)

	var target : Vector3
	if plan.status_is_init():
		target = agent.get_downish_v3() # looks downish
		_old_gravity_scale = agent.gravity_scale
		agent.gravity_scale = 0.0
		agent.lock_linear_axis_xyz(true) #NB
		plan.status_to_during()

	# reduce gravity until still
	# hold position and drop head down somewhat
	if plan.status_is_during():
		agent.energy += 0.1
		#print("energy:", agent.energy)
		if agent.energy > 100:
			plan.status_to_ending()
		agent.look_at_target(target, delta)

	if plan.status_is_ending():
		_end()


func _end():
	agent.lock_linear_axis_xyz(false) #NB
	agent.gravity_scale = _old_gravity_scale
	plan.status_to_ended()
