extends Area3D

func _ready() -> void:
	## Just better to see these in code damnit!

	gravity_space_override = SPACE_OVERRIDE_REPLACE
	gravity_point = true
	#gravity_point_unit_distance = 1 # no idea why
	gravity_point_unit_distance = 10
	gravity_point_center = Vector3.ZERO
	gravity = 9.8

	##TODO calc gravity from planet size and density etc.
