extends RigidBody3D

## The fish has two CollisionShape3D nodes
## One is directly under the RigidBody3D (bumper) and is
## for collision with other fish.
## The other is under an Area3D and is called 'ahead'.
## It is meant to look ahead and to the sides BUT NOT
## be part of the collisions with other fish.
##
## I am using PhysicsDirectSpaceState3D and intersect_shape
## to get whatever 'ahead' may be overlapping. HOWEVER I do
## not want it to detect the fish itself, which it is doing.
## I have tried various ways to exclude the RigidBody3D
## from the intersect process, but it just won't work.
##
## Not sure if this is a bug or my own miscomprehension.

@export var min_speed : float = 2
@export var max_speed : float = 5

@onready var water_sphere: Area3D = %water_sphere

var speed:float
var q:PhysicsShapeQueryParameters3D
var nf:Array #nearby fish


func _ready() -> void:
	speed = min_speed

	angular_damp = max_speed * mass * 100
	linear_damp = max_speed * mass * 2

	q = PhysicsShapeQueryParameters3D.new()

	## This is the problem
	## I want this fish (self) to not be detected by the
	## intersect_shape later on.
	#q.exclude.append(self.get_rid()) # is not working...
	q.exclude = [self.get_rid()]
	## ^^^^

	q.collide_with_bodies = true
	q.collision_mask = 2
	#q.shape = %senses/ahead.shape
	q.shape = %eyes.shape
	q.margin = 0


func _get_nearby_fish(state:PhysicsDirectBodyState3D):
	#q.transform = %senses/ahead.global_transform
	q.transform = %eyes.global_transform
	var ss := state.get_space_state()
	nf = ss.intersect_shape(q, 2)
	print("This should be empty:", nf)

	## Workaround for now
	if false:
		# I have to do this to exclude this fish (self) from the
		# array because the query.exclude array seems not to work.
		nf = nf.filter(func(i): return i.collider != self)
		print("nf:", nf)


func swim():
	speed = linear_velocity.length() + min_speed
	if speed > max_speed: return
	var fwd = speed * -global_basis.z
	apply_impulse(fwd)


func _physics_process(delta: float) -> void:
	var frames := Engine.get_physics_frames()
	if frames % 2 == 0:
		swim()


func _nose_level(state):
	var xform : Transform3D = global_transform
	xform.basis.y = -state.total_gravity
	xform.basis.x = -xform.basis.z.cross(-state.total_gravity)
	xform.basis = xform.basis.orthonormalized()
	global_transform = global_transform.interpolate_with(xform, speed * state.step)


func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	_nose_level(state)
	_get_nearby_fish(state)
