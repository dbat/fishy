extends RigidBody3D


enum {NONE, STOP,  QUATS, BASIS, BASIS2, OTHER}
var go:int = 0

func _ready() -> void:
	linear_damp = 0
	reset()

var keylock:=false # was getting two events per one key press
func _unhandled_key_input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.keycode == KEY_0:
			print("STOP")
			go = NONE
			keylock = false

		if keylock: return

		if event.keycode == KEY_1:
			print("Qturn_by_quat_diff!")
			go = QUATS

		elif event.keycode == KEY_2:
			print("Bturn_using_looking_at!")
			go = BASIS

		elif event.keycode == KEY_3:
			print("Bturn_by_quat_diff")
			go = BASIS2

		elif event.keycode == KEY_4:
			print("Bturn_by_quat_diff_once")
			go = OTHER

		keylock = (go != NONE)

func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	if go == NONE:
		reset()

func reset():
	angular_velocity = Vector3.ZERO
	angular_damp = 0
	basis = %start.basis
	position = Vector3.ZERO


# An awkward balance between linear and angular damping causes
# the fish to actually move up or down along g
var busy_spinning:=false
var seconds : float
func xx_physics_process(delta: float) -> void:
	if go == NONE or go == STOP: return

	if not busy_spinning:
		var T : Vector3
		reset()
		if go == QUATS:
			T = Qturn_by_quat_diff(2 * delta)
		if go == BASIS:
			T = Bturn_using_looking_at(2 * delta)
		if go == BASIS2:
			T = Bturn_by_quat_diff(delta)
		if go == OTHER:
			T = Bturn_by_quat_diff_once(1 * delta)
		angular_damp = 0
		print("KICK T:", T)
		DebugDraw3D.draw_ray(position, T, 5, Color.RED, 4)
		DebugDraw3D.draw_ray(position, -T, 5, Color.RED, 4)
		# ONE boost, then allow 1 second to pass before stopping.
		apply_torque_impulse(T)
		busy_spinning = true
		seconds = 0
	else:
		seconds += delta
		if is_equal_approx(seconds, 1.0):
			print("seconds:", seconds)
			print("STOPPING Q:", basis.get_rotation_quaternion())
			angular_damp = 60
			busy_spinning = false
			go = STOP


func _physics_process(delta: float) -> void:
	if go == NONE or go == STOP: return

	var T : Vector3
	if go == QUATS:
		T = Qturn_by_quat_diff(delta)
	if go == BASIS:
		T = Bturn_using_looking_at(delta)
	if go == BASIS2:
		T = Bturn_by_quat_diff(delta)
	if go == OTHER:
		T = Qturn_by_slerp(delta)


	apply_torque_impulse(T)
	DebugDraw3D.draw_ray(position, T, 5, Color.RED, 4)
	DebugDraw3D.draw_ray(position, -T, 5, Color.RED, 4)

	#DebugDraw3D.draw_arrow_ray(position, -basis.z, 2, Color.BLUE, 0.1)
	#DebugDraw3D.draw_arrow_ray(position, basis.y, 2, Color.GREEN, 0.1)




func Qturn_by_slerp(delta) -> Vector3:
	var me_Q := basis.get_rotation_quaternion().normalized()
	var to_Q :Quaternion = %target.basis.get_rotation_quaternion().normalized()
	const magic_number := 4
	var QQ := me_Q.slerp(to_Q, delta * magic_number)
	var axis = QQ.get_axis()
	var angle = QQ.get_angle()
	var T = axis * angle * (delta/magic_number)
	angular_damp += delta
	return T

## Weirdly fussy and inclined to go into endless wobbles
func Qturn_by_quat_diff(rotation_force) -> Vector3:
	var up := Vector3.UP
	var me_Q : Quaternion = basis.get_rotation_quaternion().normalized()
	#print("me_Q:", me_Q)
	var to_Q : Quaternion = %target.basis.get_rotation_quaternion().normalized()
	#print("to_Q:", to_Q)
	#var Q : Quaternion = (to_Q * me_Q.inverse()).normalized()
	var Q : Quaternion = (to_Q * me_Q.inverse()).normalized()
	#print("The Difference Quat:", Q)
	var axis := Q.get_axis()
	var angle := Q.get_angle()
	var T : Vector3 = axis * angle * rotation_force# * delta
	return T


func Bturn_by_quat_diff(rotation_force) -> Vector3:
	var up := Vector3.UP
	var to_basis: Basis = %target.basis#.orthonormalized()
	var Q = (to_basis * basis.inverse()).get_rotation_quaternion().normalized()
	var axis = Q.get_axis().normalized()
	var angle = Q.get_angle()
	var T = axis * angle * rotation_force# * delta
	return T


func Bturn_using_looking_at(rotation_force) -> Vector3:
	var up := Vector3.UP
	var target : Vector3 = %target.position #transform * %aim.position
	var to_basis: Basis
	to_basis = transform.looking_at(target, up).basis.orthonormalized()
	# doing this multiply mambo with basis works better than quaternions!
	var Q = (to_basis * basis.inverse()).get_rotation_quaternion().normalized()
	var axis = Q.get_axis()
	var angle = Q.get_angle()
	#var T = axis * angle * rotation_force * (2.0/60.0)# weirdly perfect
	var T = axis * angle * rotation_force
	return T





func Bturn_by_quat_diff_once(rotation_force) -> Vector3:
	var up := Vector3.UP
	var me_Q := basis.get_rotation_quaternion().normalized()
	print("me_Q:", me_Q)
	var to_Q :Quaternion = %target.basis.get_rotation_quaternion().normalized()
	print("to_Q:", to_Q)
	# The problem with this:
	# QQ = to_Q is trying to say be the target's quat
	# but the axis of that target quat is diff to my current axis
	# thus an impulse about that axis would fail.
	# which this does.
	var QQ := to_Q
	var axis = QQ.get_axis()
	var angle = QQ.get_angle()
	var T = axis * angle * rotation_force# * delta
	return T
