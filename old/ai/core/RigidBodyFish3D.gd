class_name RigidBodyFish3D extends RigidBody3D

## This works only because the origin of the fish is NOT
## THE SAME as the centre_of_mass. Also the position of
## the various meshes and colliders in the fish are
## offset so that the origin is closer to the head.
## These small things make a HUGE difference.

@export var speed : float = 1
@export var separation := 10.5

@export_group("Stats")
@export var ALARM := false
@export var hunger := 100.0
@export var security := 10.0
@export var energy := 100.0

@export_group("Plans")
@export var plans_collection_resource : BasePlans


enum {ALIVE, DEAD, DECAYING, DELETE}


var life_status := ALIVE
var q: PhysicsShapeQueryParameters3D
var plan: BasePlan
var agent_mesh : MeshInstance3D
var agent_material : ShaderMaterial
var _my_plans : Array[BasePlan]
var fishspace

func randomize_my_stats():
	hunger = randf_range(0, 100.0)
	security = randf_range(0, 100.0)
	energy = randf_range(0, 100.0)


func _ready() -> void:
	assert(plans_collection_resource, "There must be a Plans Collection Resource in the agent.")

	fishspace = get_parent()
	center_of_mass = %com.position

	#randomize_my_stats()

	# Code for the fish's 'eyes'
	q = PhysicsShapeQueryParameters3D.new()
	#q.exclude.append(self.get_rid()) # is not working...
	q.exclude = [self.get_rid()] # wierd, but this works!
	q.collide_with_bodies = true
	q.collision_mask = 2
	# NOTE:
	# %eyes is a CollisionShape3D and it is DISABLED!
	# This means it does NOT participate in collisions.
	# I can use it to scan for intersections with the
	# PhysicsServer stuff! Yay!
	%eyes.disabled = true
	q.shape = %eyes.shape
	q.margin = 0#.02

	# Make unique copies of all the plans for me
	plans_collection_resource.make_unique_plans_for(self)
	_my_plans = plans_collection_resource.get_plans_for(self)
	plan = _my_plans[0] # fetch the default plan from idx 0



#region #### ------------ PLANNING ------------ ###

func choose_a_plan():
	var highest_priority_plan := plan
	for p:BasePlan in _my_plans:
		if (p._is_valid()
			and p._priority() > highest_priority_plan._priority()
			):
				highest_priority_plan = p
	#print("plan:", plan.name, " hp:", highest_priority_plan.name)
	p()

	# if it's the same plan as current, bail
	if plan == highest_priority_plan : return

	# Ok, tell the old plan it's over
	if plan:
		plan._end()

	# now set the new plan and begin it.
	plan = highest_priority_plan
	print("SWITCH PLAN TO:", plan.name)
	plan._begin()


func _change_stats(): pass
#endregion



#region ### ------------ PHYSICS ------------ ###

func _process(delta: float) -> void:
	const fskip1 := 20
	if Engine.get_physics_frames() % fskip1 == 0:
		_change_stats()
		choose_a_plan()
		assert(plan, "There has to be a plan!")
		plan._run_process(delta)


func _physics_process(delta: float) -> void:
	assert(plan, "There has to be a plan!")
	plan._run_physics_process(delta)


func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	const orient_skip := 4
	const sep_skip := 3
	var frames := Engine.get_physics_frames()
	if frames % orient_skip == 0:
		_keep_oriented(state, orient_skip)

	if frames % sep_skip == 0:
		separate(state)

	if plan:
		plan._run_integrate_forces(state)
#endregion



#region ### ------------ MOVEMENTS / ANIMATIONS ------------ ###

func shader_anim_speed(time_scale:float):
	agent_material.set_shader_parameter(&"time_scale", time_scale)

func swim(delta):
	var fwd = -basis.z * delta * speed * mass
	#print("swim:", fwd)
	#apply_central_impulse(fwd)
	apply_central_force(fwd)
	# TODO get this to look good:
	shader_anim_speed(fwd.length() * delta)

func closely_follow_target(target:Vector3, delta):
	var F : Vector3 = target - position
	apply_force(F)
	# TODO get this to look good:
	shader_anim_speed(F.length() * delta)


func vaguely_head_towards(target:Vector3, delta):
	var F := Vector3.ZERO
	var d : float = abs(position.dot(target) - 1) # -2 away, 0 90deg, 1 towards
	var turn_speed : float = speed * d # faster turns when looking away
	F = position.move_toward(target, delta * turn_speed) - position
	#DebugDraw3D.draw_arrow_ray(global_position, F, 1, Color.RED,0.01,false,1)
	apply_force(F)
	# TODO get this to look good:
	shader_anim_speed(turn_speed * delta)


# only works when linear axis xyz are all locked
func look_at_target(target:Vector3, delta):
	var F : Vector3 = target - position
	apply_force(F * delta)
	# TODO get this to look good:
	shader_anim_speed(F.length() * delta)


# The only way I could find to kind of keep the fin 'up'
func _keep_oriented(state:PhysicsDirectBodyState3D, fskip1:int):
	# TODO ERROR: The target vector and up vector can't be parallel to each other.

	#var up = -state.total_gravity
	## The up vector can't be zero in looking_at
	#if up.is_zero_approx():
		#up = position - %fishspace.position

	var up = position - fishspace.position

	var b := state.transform.basis #.orthonormalized() ?
	var look := b.looking_at(-basis.z, up)
	state.transform.basis = b.slerp(
		look.orthonormalized(), # solves an error msg in slerp
		state.step * fskip1
	).orthonormalized()


# Boidish funcs
func align(state:PhysicsDirectBodyState3D): pass
func cohere(state:PhysicsDirectBodyState3D): pass
func separate(state:PhysicsDirectBodyState3D):
	var boids := _get_nearby_fish(state)
	var F := Vector3.ZERO
	var strength := 0.0
	for dict in boids:
		var other_boid = dict.collider
		#print(separation * speed * mass)#position.distance_to(boid.position))
		strength = randf_range(0, separation * speed * mass)
		#print(strength)
		F += position - other_boid.position
	#state.apply_force( F * strength )#state.step * strength)
	state.apply_force( F * strength )

func xxseparate(state:PhysicsDirectBodyState3D):
	var boids := _get_nearby_fish(state)
	for dict in boids:
		var other_boid = dict.collider
		#print(position.distance_to(boid.position))
		const boost := 10 # get this number from something ...
		var strength := randf_range(0, boost * speed * mass)
		var F : Vector3 = position - other_boid.position
		state.apply_force( F * strength )#state.step * strength)

#endregion



#region ### ------------ ASSORTED FUNCS ------------ ###


func get_downish_v3(delta := 4.0) -> Vector3:
	var a := -basis.z
	var g := get_gravity()
	var out := a.move_toward(g, delta)
	#DebugDraw3D.draw_arrow_ray(position,out,1,Color.TURQUOISE,0.01,false,60)
	return out


var scan_num := 8
func _get_nearby_fish(state:PhysicsDirectBodyState3D) -> Array :
	#q.transform = %senses/ahead.global_transform
	q.transform = %eyes.global_transform
	var ss := state.get_space_state()
	var nf := ss.intersect_shape(q, scan_num)
	#print(nf)
	if nf:
		var seen := nf.size()
		#print(nf.size())
		#print( self, " hits ", nf[0].collider)
		#if scan_num < f.size(): scan_num = f.size()
		if seen < scan_num:
			scan_num = wrapi(seen, 2, 8)
	return nf




func p():
	var s = "%s\nenergy: %3.2f\nsecurity: %3.2f" % [plan.name, energy, security]
	#print(s)
	#%t.text = ""
	%t.text = s
	#print("inv energy?", not plan._involves_energy())


func lock_linear_axis_xyz(b:bool):
	axis_lock_linear_x = b
	axis_lock_linear_y = b
	axis_lock_linear_z = b
#endregion
