class_name Behaviour extends RefCounted

# A Behaviour is the motion and behaviour and animation
# Within Behaviour, it does
#  init
#  during
#  ending
# In which the animation/physics calls are all done for this strat

var name : String

var state : int
var agent : RigidBodyFish3D
var plan : Planner

func _init(_plan, _name) -> void:
	plan = _plan
	name = _name
	agent = plan.agent


func run_process(delta: float) -> void:
	_choose_behaviour() # override this one

func run_physics_process(delta: float) -> void: pass
func run_integrate_forces(state: PhysicsDirectBodyState3D) -> void : pass

func proximity(): pass

func _choose_behaviour(): pass

func _end(): pass
