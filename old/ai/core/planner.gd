class_name Planner extends RefCounted

# Planner is a Behaviour Manager
# within an Intent it decides which behav to employ
# to complete the intent.



enum {STATUS_ENDING, STATUS_ENDED, STATUS_INIT, STATUS_DURING} # status
var status : int
var behaviour: Behaviour
var name : String
#var strats = []
var agent : RigidBodyFish3D


func _init(_agent : RigidBodyFish3D, _name) -> void:
	agent = _agent
	name = _name
	_setup_behaviours()

func _setup_behaviours(): pass


func _choose_behaviour():	pass


func end_current_behaviour():
	status_to_ending()
	behaviour._end()


func switch_behaviour_to(s):
	# make sure the current behav ends properly:
	if behaviour: behaviour._end()
	behaviour = s
	status_to_init()
	#behaviour.status_to_init() TODO

func plan_running() -> bool : return status > STATUS_ENDED

func status_is_init() -> bool : return status == STATUS_INIT
func status_is_during() -> bool : return status == STATUS_DURING
func status_is_ending() -> bool : return status == STATUS_ENDING
func status_is_ended() -> bool : return status == STATUS_ENDED
func none() -> bool : return status == STATUS_ENDED

func status_new_plan_starting() -> bool : return status > STATUS_ENDED

func status_to_init(): status = STATUS_INIT

func status_to_during(): status = STATUS_DURING
func status_to_ending(): status = STATUS_ENDING
func status_to_ended(): status = STATUS_ENDED

func xxx_to_string() -> String:
	return "Plan: %s behav: %s status:%s" % [self.get_instance_id(), behaviour, status]
