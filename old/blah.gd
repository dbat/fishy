extends Node2D

@export var bag:Array[Dictionary]

var d = {
	attribute = {
		strength = {
			value = 5,
			max = 25
			}
		}
	}

func _ready() -> void:
	print(d.attribute.strength.max)
	var attr1 = d.duplicate(true)
	var attr2 = d.duplicate(true)
	attr2.attribute.strength.max = 500

	bag.append(attr1)
	bag.append(attr2)

	#etc?
	print(bag[0].attribute.strength.max)
	print(bag[1].attribute.strength.max)
