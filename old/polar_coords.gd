class_name Polar extends Object
## Huge thanks to @efi@chitter.xyz
var altitude : float
var rotation : Quaternion

func from_cartesian(pos:Vector3, origin:Vector3 = Vector3.ZERO) -> Polar:
	altitude = (pos - origin).length()
	rotation = Quaternion(Vector3.RIGHT, origin.direction_to(pos))
	return self

func to_cartesian() -> Vector3:
	return Vector3.RIGHT.rotated(rotation.get_axis().normalized(), rotation.get_angle()) * altitude
