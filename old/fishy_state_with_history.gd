extends RigidBody3D

## Findings
##
## To get the  fish to defy gravity a little bit:
## On water_sphere Area3D
## Space override: Replace
## Point : true
## Point unit distance : 1m
## Gravity : 9.8
## AND:
## var Fb = -1  *  mass * get_gravity() # ✔ Mostly cancels gravity


#enum EMO {CONTENT, HUNGRY, AFRAID}
#enum MOVEMENT {STILL, UTURN, STRAIGHT, CIRCLING}
#enum GOAL {REST, PATROL, FINDFOOD, FLEE}
#enum STATUS {INIT, DURING, ENDING}

#var status:STATUS
#var movement:MOVEMENT
#var emo:EMO





class Intent extends RefCounted:
	enum {REST, PATROL, FINDFOOD, FLEE} # job
	enum {STATUS_INIT, STATUS_DURING, STATUS_ENDING} # status
	var job:int
	var status:int

	func _init(_job = REST, _status = STATUS_INIT) -> void:
		job = _job
		status = _status

	func _to_string() -> String:
		return "Intent: %s job: %s status:%s" % [self.get_instance_id(), job, status]

class Intentions extends RefCounted:
	var _intentions:Array#[Intent]
	var ip := 0

	func current() -> Intent:
		if _intentions.is_empty(): switch_to(Intent.REST)
		#print("Looking up current at ip:", ip, " is ", _intentions[ip])
		return _intentions[ip]

	func switch_to(job = Intent.REST):
		print("switch_to job:", job, " intentions:", _intentions)
		# make sure this kind of job is not in the list
		var hasjob = _intentions.filter(func(i): print(" filter job:", i.job); return i.job == job)
		if hasjob:
			ip = _intentions.find(hasjob[0]) # point at it, gulp!
			print("Found a job like this in list:", hasjob, " ip now:", ip)
		else:
			var new_intent = Intent.new(job)
			_intentions.append(new_intent)
			ip = _intentions.size() - 1
		print(" ip pointing at:", _intentions[ip])
		print("   job there:", _intentions[ip].job)

	func xxswitch_to(job = Intent.REST):
		print("switch_to job:", job, " intentions:", _intentions)
		if _intentions:
			var hasjob = _intentions.filter(func(i): print(" filter job:", i.job); return i.job == job)
			print("hasjob:", hasjob)
			if hasjob:
				ip = _intentions.find(hasjob[0]) # point at it, gulp!
		else:
			var new_intent = Intent.new(job)
			_intentions.append(new_intent)
			ip = _intentions.size() - 1
		print(" ip pointing at:", _intentions[ip])
		print("   job there:", _intentions[ip].job)

	func finish():
		_intentions.remove_at(ip)
		if _intentions.is_empty(): switch_to(Intent.REST)
		ip = _intentions.size() - 1




class Vertical:
	enum {UP,DOWN, LEVEL}
	var dir := 0
	var r:float


var intentions := Intentions.new()


@export var depth_band := Vector2(11,15)
@export var start_depth : float = 12
@export var noise : NoiseTexture2D
@export var min_speed : float = 0
@export var max_speed : float = 0
@export var ALARM := false
@export var hunger := 100.0
@export var security := 10.0
@export var energy := 100.0

@onready var water_sphere: Area3D = %water_sphere

var data:PackedByteArray
var speed:float
var q:PhysicsShapeQueryParameters3D

var dead := false

var funcmap := {
	Intent.REST : Callable(self,"rest"),
	Intent.PATROL : Callable(self,"patrol"),
	Intent.FINDFOOD : Callable(self,"find_food"),
	Intent.FLEE : Callable(self,"flee")
}
var job_func : Callable


func _ready() -> void:
	print("ME:", self)
	if noise:
		await noise.changed
		var img : Image = noise.get_image()
		data = img.get_data()

	# Start fish off chilling
	#print(funcmap)
	intentions.switch_to(Intent.REST)
	#job_func = lookup_job_func(intentions.current().job)

	speed = min_speed

	angular_damp = max_speed * mass * 100
	linear_damp = max_speed * mass * 2

	q = PhysicsShapeQueryParameters3D.new()
	q.exclude.append(%s_core.get_rid())
	q.collide_with_bodies = true
	q.collision_mask = 2
	q.shape = %fish_sense.shape
	q.margin = 0.02

var boost := false
## For now space will boost the fish forwards
func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.keycode == KEY_SPACE:
			boost = true

#var i:int = 0
## Right now this uses a 1d noise texture to get some
## numbers varying from -n.0 to +n.0 (whatever n is)
## to give the fish some visual up and down floatiness
#func get_buoyancy(state)->Vector3:
	#var n : float = 0
	#if noise:
		#n = data.decode_s8(i) # yay has negative numbers!
		#i = wrapi(i + 1, 0, data.size())
		##n = remap(n, 0, 255, 0, mass/100.)
		#print(n)
#
	#var fish_total_volume:float = fish_volume + n
	#var water_vol_displaced :float = fish_total_volume # archimedes
	#var water_weight :float = water_density * water_vol_displaced
	#var Fb = -1 * water_weight * state.total_gravity#.normalized()
#
	#return Fb


func nose_up_down(r:float):
	var T = basis.x.normalized()
	T = (T * r/2 ).normalized()
	apply_torque_impulse(T)


func nose_left_right(r:float):
	var T = get_gravity().normalized() #basis.y.normalized()
	T = (T * r/2).normalized()
	apply_torque_impulse(T)


func swim():
	speed = linear_velocity.length() + min_speed
	#print(speed)
	if speed > max_speed: return
	if intentions.current().job == Intent.FLEE: speed = max_speed
	var fwd = speed * -global_basis.z
	apply_impulse(fwd)
	print("go fwd:", fwd)

func stop():pass


func lookup_job_func(job) -> Callable:
	var action : Callable
	action = funcmap[job]
	#print("funcmap:", funcmap)
	#print("job_func:", action)
	#_: assert(false, "Bad callable in class Intent")
	return action



var i:int=0
func decide_up_or_down() -> Vertical:
	var V := Vertical.new()
	var n : float = 0
	if noise:
		n = data.decode_u8(i)
		i = wrapi(i + 1, 0, data.size())
		n = remap(n, 0, 255, 0, TAU)

	V.dir = V.LEVEL
	V.r = 0
	if n < TAU/3.0 :
		V.dir = V.DOWN
		V.r = n
	elif n > PI:
		V.dir = V.UP
		V.r = n
	return V


func _process(delta: float) -> void:
	var frames := Engine.get_process_frames()

	if frames % 2 == 0: # every 2 frames
		hunger -= 1
		energy -= 0.2
		security -= 1

		decide_what_to_do()


func decide_what_to_do():
	#await get_tree().create_timer(4.0).timeout
	print("DECIDE. job:", intentions.current().job)
	if ALARM:
		intentions.switch_to(Intent.FLEE)
	else:
		if security < 20:
			if intentions.current().job != Intent.PATROL \
			and intentions.current().status != Intent.STATUS_DURING:
				intentions.switch_to(Intent.PATROL)
				print("SWITCHED TO PATROL:", intentions.current().job)
		#elif energy > 30 and hunger < 20:
			#intentions.switch_to(Intent.FINDFOOD)
		#elif energy < 30:
			#intentions.switch_to(Intent.REST)

	#print("In decide what to do: job:", intentions.current().job)
	job_func = lookup_job_func(intentions.current().job)



func rest(delta, frames):
	pass#print("resting")
func flee(delta, frames):pass
func find_food(delta, frames):pass
func patrol(delta, frames):
	print("patrolling")
	var ci := intentions.current()
	if ci.status == Intent.STATUS_INIT:
		ci.status = Intent.STATUS_DURING

	if ci.status == Intent.STATUS_DURING:
		swim()
		security += 0.01
		if security > 100:
			security = 100
			ci.status = Intent.STATUS_ENDING

	if ci.status == Intent.STATUS_ENDING:
		intentions.finish()


func _physics_process(delta: float) -> void:
	var frames := Engine.get_physics_frames()
	if frames % 2 == 0:
		if job_func.is_valid():
			#print("Calling:", job_func)
			job_func.call(delta, frames)



func _nose_level(state):
	var xform : Transform3D = global_transform
	xform.basis.y = -state.total_gravity
	xform.basis.x = -xform.basis.z.cross(-state.total_gravity)
	xform.basis = xform.basis.orthonormalized()
	#global_transform = xform
	global_transform = global_transform.interpolate_with(xform, speed * state.step)


var nf:Array
func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	_nose_level(state)
	_get_nearby_fish(state)


func _get_nearby_fish(state:PhysicsDirectBodyState3D):
	q.transform = global_transform
	var ss := state.get_space_state()
	nf = ss.intersect_shape(q, 2)


func _cohere(delta:float):
	for f in nf:
		var other_fish = f.collider
		#global_transform = other_fish.global_transform
		global_transform = global_transform.interpolate_with(other_fish.global_transform, speed * delta)

#var xform = align_with_y(global_transform, -state.total_gravity)
#func align_with_y(xform, new_y):
	#xform.basis.y = new_y
	#xform.basis.x = -xform.basis.z.cross(new_y)
	#xform.basis = xform.basis.orthonormalized()
	#return xform

#var crash_damping:=false
#func xx_on_body_entered(body: Node) -> void:
	#if body == %s_core: return # ignore the planet itself
	##print(self, " has hit ", body)
	#crash_damping = true
	#body.crash_damping = true
