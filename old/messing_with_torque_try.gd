extends RigidBody3D

func _ready() -> void:
	angular_damp = 0
	linear_damp = 0
	center_of_mass = %com.position

func done_turning() -> bool:
	return angular_velocity.is_zero_approx()

# An awkward balance between linear and angular damping causes
# the fish to actually move up or down along g
func xx_physics_process(delta: float) -> void:
	var frames : int = Engine.get_physics_frames()
	if frames % 2 == 0:
		if linear_damp > 5:
			linear_damp = 0
			var I = -basis.z * delta
			#print(I)
			apply_central_impulse(I)
		linear_damp += 0.5
		#print(linear_damp)
		#angular_damp = 50
		#angular_damp = clampf(angular_damp + 1, 0, 60)
		#var T := follow(delta * gravity_scale) # seems to do the fin up as well!
		#apply_torque_impulse(T)

	#var target : Vector3 = transform * %aim.position
	DebugDraw3D.draw_arrow(position, target, Color.RED,0.01,true, 2)

func _physics_process(delta: float) -> void:
	#var frames : int = Engine.get_physics_frames()
	#if frames % 2 == 0:
		#if linear_damp > 5:
			#linear_damp = 0
			#var I = -basis.z * delta
			##print(I)
			#apply_central_impulse(I)
	#if true:#frames % 3 == 0:
		#var g := get_gravity()
		#var negg: = -g.normalized()
		#var up := negg
		##branchless way to prevent an error if in 0 gravity
		##up += basis.y * float(up == Vector3.ZERO)
		#if linear_damp > 30:
			#linear_damp = 5
			##var F = transform * %aim.position# * delta
#
			## force from pos to com
			## heads along that line ok
			##var F = center_of_mass
			##apply_force(F)
			##DebugDraw3D.draw_arrow_ray(position, F, 2, Color.RED, 0.001,false, 1)

	linear_damp = 30
	angular_damp = 20
	var F = (transform * %aim.position) - position
	apply_force(F * 4)
	DebugDraw3D.draw_arrow_ray(position, F, 2, Color.RED, 0.001,false)

	#DebugDraw3D.draw_arrow_ray(position, target, 2, Color.RED, 0.001,false)

	#print(basis.y.dot(get_gravity()))
	var d := get_gravity().normalized().dot(basis.y.normalized())
	if d > -0.02:
		print("dot:", d)
		if false:
			d = remap(d, 0, 1, PI/2, PI)
			var q := Quaternion(-basis.z, d)
			F = Vector3.RIGHT
			#F = (transform * %fin.position) - (transform * %finfrom.position)
			#F = (transform * %fin.position + transform * Vector3.RIGHT)# - position
			#F = transform * (%fin.position - %finfrom.position)
			#var f1 = (transform * %fin.position)
			#var f2 = (transform * (Vector3.LEFT +  %fin.position))
			#var f2 = (transform * %finfrom.position) - f1
			#F = f1 - f2
			#DebugDraw3D.draw_arrow_ray((transform * %finfrom.position), F, 1.0, Color.BLUE_VIOLET, 0.01, false, 2)
			DebugDraw3D.draw_arrow_ray(position, F, 0.2, Color.BLUE_VIOLET, 0.001, false)
			#apply_torque_impulse( basis.z * -q.get_angle() )
			#F = (transform * %fin.position) - position
			F = F * q.get_angle() * delta
			#F = F * delta * d
			#print(rad_to_deg( q.get_angle() ))
			apply_force(F, to_global(%finfrom.position))
		if true:
			d = remap(d, -0.02, 1, PI/2, PI)
			d = d * delta
			F = F.normalized()
			var q := Quaternion(F, d)
			q = q.normalized()
			var nF = q.get_angle() * delta
			apply_torque_impulse( F * nF)



var target : Vector3
func xx_integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	var frames : int = Engine.get_physics_frames()
	if frames % 4 == 0:
		var g := get_gravity()
		var negg: = -g.normalized()
		var up := negg
		#branchless way to prevent an error if in 0 gravity
		up += basis.y * float(up == Vector3.ZERO)
		target = -basis.z #transform * %aim.position
		#target = (transform * %aim.position) - position
		var to_xform = transform.looking_at(target, up)
		transform = transform.interpolate_with(to_xform, 0.5)



func swim(delta):
	var I = -basis.z.normalized() * delta
	apply_impulse(I)


#const magic_number := 8
#func follow(delta) -> Vector3:
	#var g := get_gravity()
	#var negg: = -g.normalized()
	#var up := negg
	##branchless way to prevent an error if in 0 gravity
	##up += basis.y * float(up == Vector3.ZERO)
#
	#var target : Vector3 = transform * %aim.position
	#print(target)
	#var to_basis: Basis
	#to_basis = transform.looking_at(target, up).basis
	## doing this multiply mambo with basis works better than quaternions!
	#var Q := (to_basis * basis.inverse()).get_rotation_quaternion().normalized()
	##var to_Q := to_basis.get_rotation_quaternion().normalized()
	##var Q := basis.get_rotation_quaternion().normalized()\
##		.slerp(to_Q, delta * magic_number)
	#var axis := Q.get_axis()
	#var angle := Q.get_angle()
	#var T = axis * angle * delta
	#return T




	#Basis Basis::looking_at(const Vector3 &p_target, const Vector3 &p_up, bool p_use_model_front) {
	##ifdef MATH_CHECKS
			  #ERR_FAIL_COND_V_MSG(p_target.is_zero_approx(), Basis(), "The target vector can't be zero.");
			  #ERR_FAIL_COND_V_MSG(p_up.is_zero_approx(), Basis(), "The up vector can't be zero.");
	##endif
			  #Vector3 v_z = p_target.normalized();
			  #if (!p_use_model_front) {
						 #v_z = -v_z;
			  #}
			  #Vector3 v_x = p_up.cross(v_z);
	##ifdef MATH_CHECKS
			  #ERR_FAIL_COND_V_MSG(v_x.is_zero_approx(), Basis(), "The target vector and up vector can't be parallel to each other.");
	##endif
			  #v_x.normalize();
			  #Vector3 v_y = v_z.cross(v_x);
	#
			  #Basis basis;
			  #basis.set_columns(v_x, v_y, v_z);
			  #return basis;

#Transform3D Transform3D::looking_at(const Vector3 &p_target, const Vector3 &p_up, bool p_use_model_front) const {
##ifdef MATH_CHECKS
		  #ERR_FAIL_COND_V_MSG(origin.is_equal_approx(p_target), Transform3D(), "The transform's origin and target can't be equal.");
##endif
		  #Transform3D t = *this;
		  #t.basis = Basis::looking_at(p_target - origin, p_up, p_use_model_front);
		  #return t;
#}
