extends Node3D

func _physics_process(delta: float) -> void:
	turn(delta)

func turn(delta):
	var currQ := basis.get_rotation_quaternion()
	var curr_angle := currQ.get_angle()
	# Let's keep adding some angle to current's angle
	var toQ := Quaternion(Vector3.UP, currQ.get_angle() + PI/8)
	# find the difference - I hope. Who knows?
	var diffQ := toQ.normalized() * currQ.inverse()
	var diff_angle:= diffQ.get_angle()
	print(" curr's angle:", rad_to_deg(curr_angle))
	print(" diffQ's ANGLE:", rad_to_deg( diff_angle))
	print(" SUM:", rad_to_deg(curr_angle + diff_angle ))
	print(" TO angle:", rad_to_deg( toQ.get_angle()), " SHOULD EQUAL SUM")
	print()
	%pointy.rotate_y(diff_angle)
	assert(is_equal_approx(diff_angle, PI/8),"STOP")









func turn2(delta):
	var currQ := basis.get_rotation_quaternion()
	print("curr angle:", rad_to_deg( currQ.get_angle()))

	var target : Vector3 = transform * %aim.position

	var look_basis : Basis = transform.looking_at(target, Vector3.UP).basis
	var lookQ := look_basis.get_rotation_quaternion()
	print("look angle:", rad_to_deg( lookQ.get_angle()))

	var QQ := lookQ.normalized() * currQ.inverse()

	var angle:= QQ.get_angle()

	print(" going to rot by:", rad_to_deg( angle))
	%pointy.rotate_y(angle)
