extends RigidBody3D

@export var air_bladder_vol : float = 0.01
@export var water_density : float = 1024
@export var band := Vector2(11,15)
@export var noise : NoiseTexture2D
@export var speed : float = 0

@onready var water_sphere: Area3D = %water_sphere

var fish_volume : float
var data:PackedByteArray

func _ready() -> void:
	#$"../fishy2/cam".make_current()
	# Make the fish as dense as water + a little bit
	var fish_density = water_density + 10# + (water_density/10.0)
	# Then find the volume via that.
	fish_volume = mass/fish_density
	if noise:
		await noise.changed
		var img : Image = noise.get_image()
		data = img.get_data()

	# Start fish off going forwards. I guess.
	var global_fwd : Vector3 = -global_basis.z# * Vector3.FORWARD # do not fuck with
	apply_impulse(speed * global_fwd)


var i:int = 0
## Right now this uses a 1d noise texture to get some
## numbers varying from -n.0 to +n.0 (whatever n is)
## to give the fish some visual up and down floatiness
func get_bouyancy(state) -> Vector3:
	var n : float = 0
	if noise:
		n = data.decode_s8(i) # yay has negative numbers!
		i = wrapi(i + 1, 0, data.size())
		n = remap(n, 0, 255, 0, air_bladder_vol)
		print(n)

	var fish_total_volume:float = fish_volume + n
	var water_vol_displaced :float = fish_total_volume # archimedes
	var water_weight :float = water_density * water_vol_displaced
	var Fb = -1 * water_weight * state.total_gravity#.normalized()
	return Fb

var boost : Vector3
## For now space will boost the fish forwards
func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.keycode == KEY_SPACE:
			# This is the magic 'go forward' formula:
			var global_fwd : Vector3 = -global_basis.z
			#var global_fwd : Vector3 = global_basis * Vector3.FORWARD
			boost = speed * global_fwd


func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	# Fb is buoyancy force ish
	var Fb := get_bouyancy(state)

	state.apply_central_force(Fb)

	if boost > Vector3.ZERO:
		#print("BOOST:", boost)
		apply_impulse(boost)
		boost = Vector3.ZERO

	if crash_damping and linear_damp == 0:
		linear_damp = lerpf(1, 0, state.step)
		crash_damping = int(true) * linear_damp

	#NEARLY WORKS - but flickers a lot at first
	#In this case we're finding the rotation from the current "up" vector (the Y basis vector) to the surface normal which we want to be the new "up" vector.
	#var b_rotation := Quaternion(transform.basis.y, Fb)
	#transform.basis = Basis(b_rotation * basis.get_rotation_quaternion())

	# Totally works
	# I'm guessing _integrate_forces gives one a small peek into the
	# FUTURE of the body. The Fb I calculated here (and applied) is
	# the force that will be applied *after* this func is done.
	# Hence global_transform is NOW and Fb is NEXT
	var xform = align_with_y(global_transform, Fb) # so Fb is a future vector
	global_transform = xform#global_transform.interpolate_with(, 12 * state.step)
	#print(12 * state.step)

func align_with_y(xform, new_y):
	xform.basis.y = new_y
	xform.basis.x = -xform.basis.z.cross(new_y)
	xform.basis = xform.basis.orthonormalized()
	return xform

var crash_damping:=false
func _on_body_entered(body: Node) -> void:
	if body == %s_core: return
	#print(self, " has hit ", body)
	crash_damping = true
	body.crash_damping = true
