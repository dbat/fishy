class_name Fishy extends RigidBody3D

@export var speed : float = 1

func _ready() -> void:
	angular_damp = 60
	linear_damp = 60
	center_of_mass = %com.position

#
#func done_turning() -> bool:
	#return angular_velocity.is_zero_approx()
#

## I am splitting up the calls to anything that will
## apply a force because each one adjusts the damping
## and that needs a full frame to take effect before
## the next change will do anything.
func _physics_process(delta: float) -> void:
	move_target(delta)
	var frames := Engine.get_physics_frames()
	if frames % 2 == 0:
		swim(delta)
	if frames % 3 == 0:
		closely_follow_target(%follow)
		#vaguely_head_towards(%follow, delta)


func swim(delta):
	var fwd = -basis.z * delta * speed
	linear_damp = 20
	apply_central_impulse(fwd)


## This works only because the origin of the fish is NOT
## THE SAME as the centre_of_mass. Also the position of
## the various meshes and colliders in the fish are
## offset so that the origin is closer to the head.
## These small things make a HUGE difference.
func closely_follow_target(target):
	var F : Vector3 = target.position - position
	# TODO? The further turned-away, the LESS damping i want
	# 0 should be close to damp 0
	# 1 should be close to damp 60
	linear_damp = 20
	angular_damp = 4
	apply_force(F)


func vaguely_head_towards(target, delta):
	var F : Vector3
	var d := position.dot(target.position) + 1
	var turn_speed := speed * 2
	F = position.move_toward(target.position, delta * d * turn_speed) - position
	linear_damp = 20
	angular_damp = 4
	apply_force(F)


func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	var frames := Engine.get_physics_frames()
	if frames % 2 == 0:
		## The only way I could find to kind of keep the fin up
		## Still weird. And more weird when there are more fish...
		#var d := get_gravity().normalized().dot(basis.y.normalized())
		var d := state.total_gravity.normalized().dot(basis.y.normalized()) + 1
		# -1 is fin up IS OK (180degs)
		#  0 is fin 90 deg - i.e. heading downwards/upwards
		# +1 is fin 0 degs - i.e. upside down (basis y facing gravity down)

		#var r = get_gravity().normalized().angle_to(basis.y)
		var r = state.total_gravity.normalized().angle_to(basis.y)
		#print("d:", d, " r:", rad_to_deg(r))
		#var r = get_gravity().normalized().signed_angle_to(basis.y, basis.z)
		var F = basis.z
		var nF = PI - r
		#nF *= state.step * 1 # 8 is same as the frames mod
		angular_damp = 5
		#state.angular_velocity += F * nF * state.step * speed * d
		state.angular_velocity = F * nF * state.step * d

		#var tto := transform.rotated(F, nF)#orthonormalized()
		#transform = transform.interpolate_with(tto, state.step)


var w:float = 0
func move_target(delta):
	## Makes the target go up and down
	var norm : Vector3 = %follow.basis.y#.normalized()
	w += delta/1
	var v = (0.2 * sin(w) + 0.4) * norm
	v.x = 0.2 * sin(w)
	#print(sin(w))
	%follow.position = v
