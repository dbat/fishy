extends RigidBody3D

func _ready() -> void:
	angular_damp = 0
	linear_damp = 0



func _physics_process(delta: float) -> void:
	DebugDraw3D.draw_arrow_ray(position, -basis.z, 2, Color.BLUE, 0.1)
	DebugDraw3D.draw_arrow_ray(position, basis.y, 2, Color.GREEN, 0.1)


# Seems to work
func fin_up(delta):
	var g := get_gravity()
	DebugDraw3D.draw_arrow_ray(global_position, 6*g, 6, Color.YELLOW, 0.01)
	var negg: = -g
	DebugDraw3D.draw_arrow_ray(global_position, 6*negg, 6, Color.AQUA, 0.01)
	var currQ := basis.get_rotation_quaternion()
	var xform : Transform3D = transform
	xform.basis.y = negg
	xform.basis.x = -xform.basis.z.cross(xform.basis.y)
	xform.basis = xform.basis.orthonormalized()
	var upQ := xform.basis.get_rotation_quaternion()

	var QQ := upQ * currQ.inverse()

	var axis := QQ.get_axis()
	var angle:= QQ.get_angle()
	var T = axis  * angle# * delta
	angular_velocity = T # this works better than apply torque



var rekick:bool = true
func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	var frames : int = Engine.get_physics_frames()
	sleeping = false
	if frames % 10 == 0 and rekick:
		print("KICK")# a vel:", angular_velocity)
		#angular_velocity = Vector3.ZERO
		follow(state.step) # seems to do the fin up as well!
		rekick = false
		return
	if frames % 10 == 0 and not rekick:
		#angular_velocity = Vector3.ZERO
		rekick = true


# Learned :
#   var Q1 := Quaternion(curr_basis)
#   var Q2 := Quaternion(look_basis)
#   var Q := Q2 * Q1.inverse()
# Is the right order!
func follow(delta):
	var g := get_gravity()
	var negg: = -g.normalized()
	var target : Vector3 = transform * %aim.position
	DebugDraw3D.draw_arrow(position, target, Color.RED,0.01,true, 60)
	var look_basis : Basis = transform.looking_at(target, negg).basis
	var lookQ := look_basis.get_rotation_quaternion().normalized()
	var currQ := basis.get_rotation_quaternion().normalized()
	#var QQ := lookQ * currQ.inverse()
	var s := currQ.slerp(lookQ,0.5).normalized()
	basis = Basis(s)


# https://www.reddit.com/r/godot/comments/va4skf/accomplish_a_local_rotation_with_a_global_torque/
# https://github.com/addmix/godot_utils
func keep_upright(delta : float, body_state : PhysicsDirectBodyState3D) -> void:
	#desired up-facing vector
	var up : Vector3 = -body_state.total_gravity.normalized()
	#branchless way to prevent an error if in 0 gravity
	up += global_transform.basis.y * float(up == Vector3.ZERO)

	#use last forward vector and current desired up direction to get a close to optimal right-facing vector
	#as a note, cross product produces a vector perpendicular to the two input vectors, I've made a visualizer that you can use on my Itch.io https://addmix.itch.io/vector-math-visualizer
	var last_direction := -basis.z
	var right_axis := up.cross(last_direction)

	#construct Basis with the right, up, and forward (xyz) principal vectors.
	#we make sure we orthonormalize the basis, because there can be slight errors
	var desired_orientation := Basis(right_axis, up, last_direction).orthonormalized()

	#now, here's the really useful code, doing some transform math, we get a rotation basis that will rotate the current basis into the desired orientation, and we will cast that Basis to a Quaternion, as they are nicer to convert to torques.
	var force : Vector4 = quat_to_axis_angle(
			(desired_orientation * global_transform.basis.inverse()).get_rotation_quaternion())

	#this function returns a unit vector4 (length of 1), in the x, y, z values, and a length in the w value, so we reconstruct the unit vector and multiply it by the length.
	#torques work as if you have an axis to rotate about, and an amount to rotate.
	var rotation_force := 0.1
	apply_torque_impulse(Vector3(force.x, force.y, force.z) * force.w * rotation_force * delta)

static func quat_to_axis_angle(quat : Quaternion) -> Vector4:
	var axis_angle := Vector4(0, 0, 0, 0)

	if quat.w > 1: #if w>1 acos and sqrt will produce errors, this cant happen if quaternion is normalised
		quat = quat.normalized()

	var angle = 2.0 * acos(quat.w)
	axis_angle.w = sqrt(1 - quat.w * quat.w) #assuming quaternion normalised then w is less than 1, so term always positive.

	if is_equal_approx(axis_angle.w, 0.0): #test to avoid divide by zero, s is always positive due to sqrt
		axis_angle.x = quat.x
		axis_angle.y = quat.y
		axis_angle.z = quat.z
	else:
		axis_angle.x = quat.x / axis_angle.w
		axis_angle.y = quat.y / axis_angle.w
		axis_angle.z = quat.z / axis_angle.w

	return axis_angle




var wtf:float = 0
func LASTGOODWORKINGfollow(delta):
	print("AT START ANG VEL:", angular_velocity)
	var g := get_gravity()
	var negg: = -g.normalized()
	# debug draw gravity and antigravity
	#DebugDraw3D.draw_arrow_ray(global_position, 6*g, 6, Color.YELLOW, 0.01)
	#DebugDraw3D.draw_arrow_ray(global_position, 6*negg, 6, Color.AQUA, 0.01)

	var currQ := basis.get_rotation_quaternion()
	#print("basis:", basis)
	#print("current Q angle:", rad_to_deg( currQ.get_angle()) )
	#var target : Vector3 = basis.x.rotated(basis.y, deg_to_rad(1))
	wtf += 10
	var teenyweenylittleangle := deg_to_rad(wtf)
	print("wtf degrees:", wtf)
	#print("TEST:", Vector3(1,0,0).rotated(Vector3.UP, PI))#gives (-1, 0, 0)
	#print("TEST:", Vector3(1,0,0).rotated(Vector3.UP, teenyweenylittleangle))#gives (-1, 0, 0)


	var target : Vector3 = position + (-basis.z)


	var dir: Vector3 = target - position # get point direction relative to pivot
	dir = Quaternion(Vector3.UP, teenyweenylittleangle) * dir # rotate it
	target = dir + position #point = dir + pivot; // calculate rotated point



	print("target:", target, " from pos:", position)
	DebugDraw3D.draw_arrow(position, target, Color.RED,0.01,true, 60)
	%vis.position = %fishspace.transform * target
	#print("angle from z to target:", rad_to_deg( target.angle_to(-basis.z)))

	var look_basis : Basis = basis.looking_at(target - position, Vector3.UP)#negg)
	#var look_basis : Basis = transform.looking_at(target, Vector3.UP).basis

	print("my basis:", basis)
	print("Look basis:", look_basis)
	var lookQ := look_basis.get_rotation_quaternion()
	print("look Q angle:", rad_to_deg( lookQ.get_angle()) )

	var QQ := lookQ.normalized()#(lookQ.normalized() * currQ.inverse().normalized())
	#var QQ := currQ * lookQ.inverse()

	var axis := QQ.get_axis()
	print("AXIS:", axis)
	var angle:= QQ.get_angle()
	print("QQ angle:", rad_to_deg(angle))
	#return
	var T := axis * angle

	angular_damp = 0
	angular_velocity = T # this works better than apply torque
	print("AT END ANG VEL:", angular_velocity)


	# Unknown why, but this torque impulse does not perform
	# properly. It's way too strong. Even done only every 60 frames.
	#apply_torque_impulse(T)



func dunno_wtf_follow(delta):
	var g := get_gravity().normalized()
	var negg: = -g

	var yaw_angle : float = PI/2#0.5#+1
	var pitch_angle : float = 0
	var roll_angle : float

	#var zQ := z_steer_basis.get_rotation_quaternion()
	#var rollQ := zQ * origQ.inverse()
	var origQ := basis.get_rotation_quaternion().normalized()
	#print(" orig angle:",rad_to_deg(origQ.get_angle()))

	var newQ : Quaternion

	var rotated := basis.rotated(basis.y, yaw_angle)
	var yawQ := Quaternion(rotated).normalized()


	#var yawQ := Quaternion(basis.y.normalized(), yaw_angle).normalized()
	#func vector_angle_difference_from_quaternions(quat1: Quaternion, quat2: Quaternion) -> Vector3:
		#var diff =  quat1 * quat2.inverse()
		#diff = diff.normalized().get_euler()
		#return Vector3(diff.x, diff.y, diff.z)
	#object.angular_velocity = vector_angle_difference_from_quaternions(quaternion, object.quaternion) * rotational_strength * delta

	var diff := yawQ * origQ.inverse()
	#var diff := origQ * yawQ.inverse()
	#var plus := origQ * yawQ
	#diff = origQ.slerp(yawQ,1.0)
	newQ = diff
	#print(newQ)
	var axis := newQ.normalized().get_axis()
	var angle:= newQ.get_angle()
	print("  FROM:", rad_to_deg(origQ.get_angle()), " spin by:",rad_to_deg(angle))

	#DebugDraw3D.draw_arrow_ray(global_position, axis,2,Color.DEEP_PINK, 0.01)
	#angle = wrapf(angle,0,PI)

	var T = axis * angle# * delta

	angular_velocity = T # this works better than apply torque
	#print(basis.y)
	#angular_damp = 0
	#apply_torque_impulse(T)

func xxfin_up(delta):
	pass



func swim():
	var speed = 0.1
	var fwd = speed * -basis.z
	linear_damp = 4
	apply_impulse(fwd)




func ok_version_of_but_also_fails_follow(delta):
	var g := get_gravity()
	var negg: = -g
	# debug draw gravity and antigravity
	DebugDraw3D.draw_arrow_ray(global_position, 6*g, 6, Color.YELLOW, 0.01)
	DebugDraw3D.draw_arrow_ray(global_position, 6*negg, 6, Color.AQUA, 0.01)

	var currQ := basis.get_rotation_quaternion()
	var target : Vector3 = %target.position # even tho it's not in the same node?
	var look_basis := basis.looking_at(target, negg)
	var lookQ := look_basis.get_rotation_quaternion()

	var QQ := lookQ * currQ.inverse()
	var axis := QQ.get_axis()
	var angle:= QQ.get_angle()
	var T = axis * angle
	angular_velocity = T # this works better than apply torque






#
#
#
## arc_from arc_to - no idea how this works.
#func arc_mystery  ():
	##var look_basis := basis.looking_at(%target.position, basis.y)
	#var arc_from := global_position
	#var arc_to : Vector3 = %target.global_position
#
	#var Q := Quaternion(arc_from, arc_to)
	#print("Q:", Q)
	#print("arc_from:", arc_from)
	#print("arc_to:", arc_to)
	#var axis := Q.get_axis()
	#var angle:= Q.get_angle()
	#print("axis:", axis)
	#print("angle:", angle)
	#var T = axis  * angle
	#angular_velocity = T
	#print(T)



func fails_follow(delta):
	var g := get_gravity().normalized()
	var negg: = -g
	# debug draw gravity and antigravity
	DebugDraw3D.draw_arrow_ray(global_position, 6*g, 6, Color.YELLOW, 0.01)
	DebugDraw3D.draw_arrow_ray(global_position, 6*negg, 6, Color.AQUA, 0.01)

	DebugDraw3D.draw_arrow_ray(%Area3D.position, 6*negg, 6, Color.VIOLET, 0.01)
	var currQ := basis.get_rotation_quaternion()
	var target : Vector3 = to_local(%target.global_position) - position
	var up := negg
	var look_basis := basis.looking_at(target, up)
	var lookQ := look_basis.get_rotation_quaternion()

	var QQ := lookQ * currQ.inverse()
	var axis := QQ.get_axis()
	var angle:= QQ.get_angle()
	var T = axis * angle
	angular_damp = 10
	angular_velocity = T # this works better than apply torque
	#apply_torque_impulse(T)


func more_fail_follow(delta):
	var g := get_gravity().normalized()
	var negg: = -g
	# debug draw gravity and antigravity
	#DebugDraw3D.draw_arrow_ray(global_position, 6*g, 6, Color.YELLOW, 0.01)
	#DebugDraw3D.draw_arrow_ray(global_position, 6*negg, 6, Color.AQUA, 0.01)

	#DebugDraw3D.draw_arrow_ray(%Area3D.position, 6*negg, 6, Color.VIOLET, 0.01)
	var currQ := basis.get_rotation_quaternion()
	var target : Vector3 = to_local(%target.global_position)# - position
	DebugDraw3D.draw_arrow_ray(global_position, target, 2, Color.RED, 0.01)

	#var xform := transform
	#xform.basis.z = target - position
	#xform.basis.x = basis.x
	#xform.basis.y = xform.basis.x.cross(xform.basis.z)
	##xform.basis = xform.basis.orthonormalized()
#
	#var up := xform.basis.x.cross(-basis.z)
	#DebugDraw3D.draw_arrow_ray(global_position, up, 2, Color.VIOLET, 0.01)
	#return

	var look_basis := basis.looking_at(target, negg)
	var lookQ := look_basis.get_rotation_quaternion()

	var QQ := lookQ * currQ.inverse()
	var axis := QQ.get_axis()
	var angle:= QQ.get_angle()
	var T = axis * angle# * delta
	angular_velocity = T # this works better than apply torque

	#angular_damp = 10
	#apply_torque_impulse(T)



func looking_at_is_evil_follow(delta):
	var g := get_gravity().normalized()
	var negg: = -g
	# debug draw gravity and antigravity
	#DebugDraw3D.draw_arrow_ray(global_position, 6*g, 6, Color.YELLOW, 0.01)
	#DebugDraw3D.draw_arrow_ray(global_position, 6*negg, 6, Color.AQUA, 0.01)

	#DebugDraw3D.draw_arrow_ray(%Area3D.position, 6*negg, 6, Color.VIOLET, 0.01)
	#basis = basis.orthonormalized()
	var currQ := basis.get_rotation_quaternion()
	var target : Vector3 = %aim.position
	#print(target)
	DebugDraw3D.draw_arrow(global_position, to_global(target),Color.RED, 0.01)

	#var xform := transform
	#xform.basis.z = target - position
	#xform.basis.x = basis.x
	#xform.basis.y = xform.basis.x.cross(xform.basis.z)
	##xform.basis = xform.basis.orthonormalized()
#
	#var up := xform.basis.x.cross(-basis.z)
	#DebugDraw3D.draw_arrow_ray(global_position, up, 2, Color.VIOLET, 0.01)
	#return
	linear_damp = 0
	var dir : Vector3 = position + target
	var newpos : Vector3 = dir
	print("my mf pos:", position)
	%vis.position = newpos
	var look_basis := basis.looking_at(newpos, negg)
	var lookQ := look_basis.get_rotation_quaternion()


	var QQ := lookQ * currQ.inverse()
	##var QQ := currQ.normalized().slerpni(lookQ.normalized(), 1).normalized()
	var axis := QQ.get_axis().normalized()
	#DebugDraw3D.draw_arrow_ray(global_position, axis,2,Color.DEEP_PINK, 0.01)
	var angle:= QQ.get_angle()
	#print(" angle:", angle)
	var T = axis * angle * delta

	#angular_velocity = T # this works better than apply torque
	#print(basis.y)
	angular_damp = 0
	apply_torque_impulse(T)





func some_slerp_some_not_close_but_meh_follow(delta):
	var g := get_gravity().normalized()
	var negg: = -g
	# debug draw gravity and antigravity
	#DebugDraw3D.draw_arrow_ray(global_position, 6*g, 6, Color.YELLOW, 0.01)
	#DebugDraw3D.draw_arrow_ray(global_position, 6*negg, 6, Color.AQUA, 0.01)

	#DebugDraw3D.draw_arrow_ray(%Area3D.position, 6*negg, 6, Color.VIOLET, 0.01)
	#basis = basis.orthonormalized()
	#var target : Vector3 = %aim.position
	#print(target)
	#DebugDraw3D.draw_arrow(global_position, to_global(target),Color.RED, 0.01)

	#linear_damp = 0
	var yaw_angle : float = PI/2#0.5#+1
	var pitch_angle : float = 0
	var roll_angle : float

	#var z_steer_basis := basis.looking_at(-basis.z, negg)
	#var zQ := z_steer_basis.get_rotation_quaternion()
#
	#var rollQ := zQ * origQ.inverse()
	var origQ := basis.get_rotation_quaternion()
	var newQ : Quaternion

	#var yaw_basis := basis.rotated(basis.y, yaw_angle)
	#var yawQ := yaw_basis.get_rotation_quaternion()
	var yawQ := Quaternion(basis.y, yaw_angle)
	#newQ = yawQ * origQ#.inverse()

	#var pitch_basis := basis.rotated(basis.x, pitch_angle)
	#var pitchQ := pitch_basis.get_rotation_quaternion()
#
	#newQ = pitchQ * newQ.inverse()

	newQ = origQ.slerp(origQ * yawQ, 1.0)

	var axis := newQ.get_axis().normalized()
	var angle:= newQ.get_angle()

	#DebugDraw3D.draw_arrow_ray(global_position, axis,2,Color.DEEP_PINK, 0.01)
	print(" orig angle:",rad_to_deg(origQ.get_angle()))
	print(" new angle:",rad_to_deg(angle))
	var T = axis * angle# * delta

	angular_velocity = T # this works better than apply torque
	#print(basis.y)
	#angular_damp = 0
	#apply_torque_impulse(T)
