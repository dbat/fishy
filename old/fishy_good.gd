extends RigidBody3D

@export var air_bladder_vol : float = 0.01
@export var water_density : float = 1024
@export var band := Vector2(11,15)
@export var noise : NoiseTexture2D
@export var speed : float = 0

var fish_volume : float
var data:PackedByteArray

func _ready() -> void:
	# Make the fish as dense as water + a little bit
	var fish_density = water_density + 100
	# Then find the volume via that.
	fish_volume = mass/fish_density
	if noise:
		await noise.changed
		var i : Image = noise.get_image()
		data = i.get_data()

	# Start fish off going forwards. I guess.
	linear_velocity = Vector3.FORWARD


var i:int = 0
## Right now this uses a 1d noise texture to get some
## numbers varying from -n.0 to +n.0 (whatever n is)
## to give the fish some visual up and down floatiness
func get_bouyancy(state) -> Vector3:
	var n : float = 0
	if false:#noise:
		n = data.decode_s8(i) # yay has negative numbers!
		i = wrapi(i + 1, 0, data.size())
		n = remap(n, 0, 255, 0, air_bladder_vol)
	var lvd:float = linear_velocity.length()
	n = n * lvd # Should slow the wobbling as velocity goes down
	var fish_total_volume:float = fish_volume + n
	var water_vol_displaced = fish_total_volume # archimedes
	var water_weight = water_density * water_vol_displaced
	var Fb = -1 * water_weight * state.total_gravity
	return Fb


## For now any key will boost the fish forwards
func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		# This is the magic 'go forward' formula:
		var boost = speed * (global_basis * Vector3.FORWARD)
		print("BOOST:", boost)
		apply_impulse(boost)


## https://docs.godotengine.org/en/stable/tutorials/physics/rigid_body.html#the-look-at-method
## More magic and voodoo than I can cope with here, but it works.
func look_follow(state: PhysicsDirectBodyState3D, current_transform: Transform3D, target_position: Vector3) -> void:
	# Why is this called forward_local_axis when -Z is forward? This is +X.
	var forward_local_axis: Vector3 = Vector3(1, 0, 0)

	var forward_dir: Vector3 = (current_transform.basis * forward_local_axis).normalized()
	var target_dir: Vector3 = (target_position - current_transform.origin).normalized()
	var local_speed: float = clampf(speed, 0, acos(forward_dir.dot(target_dir)))
	if forward_dir.dot(target_dir) > 1e-4:
		state.angular_velocity = local_speed * forward_dir.cross(target_dir) / state.step


func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	# Fb is buoyancy force ish
	var Fb := get_bouyancy(state)
	var a_bit_ahead = to_global(Vector3(0,0,-2)) # -2 on the local Z
	var target_position = global_transform.origin + a_bit_ahead
	look_follow(state, global_transform, target_position)

	state.apply_central_force(Fb)
