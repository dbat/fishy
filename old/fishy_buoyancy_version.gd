# Very messy older code

extends RigidBody3D

var fish_volume : float

#@export var fish_density : float = 1025
@export var air_bladder_vol : float = 0.1

@export var water_density : float = 1024
@export var noise : NoiseTexture2D


#V	= 	fluid volume  V = 4/3πr^3
#var pwater = 1.0 # pwater 1.03 * kg/m3
var data:PackedByteArray
#var fish_density:float

@onready var volume = $volume

func _ready() -> void:
	#fish_volume = (4/3) * PI * pow(volume.shape.radius,3)
	# Make the fish as dense as water + a little bit
	var fish_density = water_density + 10
	# Then find the volume via that.
	fish_volume = mass/fish_density
	print("vol:", fish_volume)
	print("fish_density:", fish_density)
	await noise.changed
	var i : Image = noise.get_image()
	print("format:", i.get_format())
	data = i.get_data()

	#var LV:Vector3 = linear_velocity
	#print(LV)
	#LV.y += -1
	#linear_velocity = LV

var i:int = 0
## Note: apply_central_force(- state.total_gravity) keeps fish still.
func old_integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	# There are no 'bands' of depth when it comes to
	# buoyancy.
	# If the fish is denser it sinks (from any depth)
	# If the fish is less dense it rises - hence bladder
	# A fish must be generally denser in order to sink, else it
	# must physically SWIM downwards.
	#i = wrapi(i + 1, 0, data.size())
	i += 1
	var n : float = data.decode_u8(i)
	#print(n)
	#n = (128.0 - n)/air_bladder_vol
	#n = (128.0 - n) / 255.
	n = remap(n, 0, 255, 0, air_bladder_vol)
	n = 0
	#print("n:", n)

	#var fish_total_volume = sign(n) * (fish_volume + n) # + air_bladder_vol)# * n
	var fish_total_volume = fish_volume + n
	var water_vol_displaced = fish_total_volume
	var water_weight = water_density * water_vol_displaced
	#var Fb = -1 * water_weight * get_gravity()#state.total_gravity
	var Fb = -1 * water_weight * state.total_gravity
	#print("water_weight:", water_weight)
	#print(n, "-->", Fb)

	#Fb = state.linear_velocity - Fb

	#print("state.total_gravity:", state.total_gravity)
	#print("get_gravity():", get_gravity())
	#print("basis:", transform.basis)

	var fwd = 2 * Vector3.FORWARD
	print("fwd:", fwd)
	fwd = fwd * transform.basis#.orthonormalized()
	print("  fwd:", fwd)

	#var la := -get_gravity().cross(fwd)
	#var rb := Basis(la, -get_gravity(), fwd).orthonormalized()
	var locg = -state.total_gravity.normalized()
	var la := locg.cross(fwd)
	var rb := Basis(la, locg, fwd).orthonormalized()

	var rotspeed = 0.1
	#transform.basis = Basis(transform.basis.get_rotation_quaternion().slerp(rb, state.step * rotspeed))

	#apply_central_force(fwd)
	#apply_central_force(Fb)

	apply_central_force(Fb + fwd)

	#print("  basis:", transform.basis)

var imp := 1000
func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	var Fb = -mass * state.total_gravity
	var foo = linear_velocity.length_squared()
	#print(foo)
	if foo < 0.01:
	#if imp == 0:
		var fwd = 3 * Vector3.FORWARD
		print("BOOST fwd:", fwd)
		fwd = fwd * transform.basis
		apply_impulse(fwd)
		#imp = 1000
	#imp = imp - 1

	#var locg = -state.total_gravity.normalized()
	#var la := locg.cross(fwd)
	#var rb := Basis(la, locg, fwd).orthonormalized()

	#var rotspeed = 0.1
	#transform.basis = Basis(transform.basis.get_rotation_quaternion().slerp(rb, state.step * rotspeed))

	apply_central_force(Fb)



















func OLD_integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	apply_central_force(- state.total_gravity)
	print(state.total_gravity)
	return

	# Density (rho p)
	#  m
	# p V
	# m kg, p kg/m3, V m3
	#
	# Pressure
	# P = p g h
	#
	# Buoyant Force
	# Fb = Masswaterdisplaced(kg) * g ->Newtons
	#
	# p of salt water = 1024 1025
	# fish are mostyly the same p as their water

	#var v = (Vector3.ZERO - position).normalized() * 200.0
	#print(v)
	#var v = Vector3(0,2,0)
	#apply_central_force(v)
	#Archimedes' principle
	#Fb = -pwater g V
	# Fb	= 	buoyant force
	# pwater	= 	fluid density
	# g	= 	acceleration due to gravity
	#V	= 	fluid volume
	#  V = 4/3πr^3
	#var pwater = 1.0 # pwater 1.03 * kg/m3
	#var g = state.total_gravity
	#var V =(4/3) * PI * pow(0.297,3)
	##var vol_air_in_bladder = 1.03 #guess this
	#var air_density = 1.03 #kg/m3
	#V = V + (air_density * air_bladder_vol)
	#var buoy = -pwater * g * V
#
	##var fish_air_bladder_m3 = 0.00797 * 10
	##var water_density = 1030
	#var fb = buoy #g * (fish_air_bladder_m3 * water_density)
	#print(fb)
	#apply_central_force(fb)

	# Volume of air bladder DISPLACES water
	# i.e. Volume of entire fish
	# To sink you must DEC the volume
	# To rise you must INC the volume
	#
	# 50 psi == 110 feet dive
	# 15 psi == atm at sea level
	#
	# boyle's law -> P : 1/v

	apply_central_force(- state.total_gravity)
	print(state.total_gravity)
	return

 #g:float = ProjectSettings.get_setting("physics/3d/default_gravity")
	var water_vol_displaced = fish_volume
	var water_weight = water_density * water_vol_displaced
	var Fb = water_weight * state.total_gravity  #9.8
	print("water_vol_displaced:", water_vol_displaced)
	print("water_weight:", water_weight)
	print("Fb:", Fb)
	apply_central_force(Fb)
	return

	var negg = -state.total_gravity
	var n : float = data.decode_u8(i)
	i = wrapi(i + 1, 0, data.size())
	n = (128.0 - n)/9.8
	var fb : Vector3 = negg * n
	#print(n, "-->", fb)
	apply_central_force(fb)
	return

	##air_bladder_vol += noise.noise.get_image( randf_range(-4,4)
	#var n : float = data.decode_u8(i)
	#i = wrapi(i + 1, 0, data.size())
	##i += 1; if i > data.size(): i = 0
	##n = 128 - n
	#var d = (Vector3.ZERO - position).length()
	#d = (10 - d)/2.0
	#n = remap(n, 0, 256, -1, 1)
	#d = d * n
	##p=p0+ρhg, Where p is the pressure at a particular depth, p0 is the pressure of the atmosphere, ρ is the density of the fluid, g is the acceleration due to gravity, and h is the depth.
	#var pressure = 1 + d
	#air_bladder_vol = 1.0/pressure # v = 1/p
	##air_bladder_vol = vol
#
	##air_bladder_vol = lerpf(air_bladder_vol, air_bladder_vol + d, n)
	##air_bladder_vol = clampf(air_bladder_vol, 0, Vfish * 2.0)
	##var V = Vfish + air_bladder_vol # up or down happens here
	## The fish needs a max and min volume!!!
	##V = clampf(V, 1, 9)
	#var V = Vfish + air_bladder_vol #lerpf(0, Vfish, air_bladder_vol)
	#print(d, "-->", air_bladder_vol, "-->", V)
	#var fb = -pwater * g * V
	#apply_central_force(fb)
