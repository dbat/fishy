class_name FishPattern extends RigidBodyFish3D


@onready var followme = %follow

var territory_plan = TerritoryPlan.new(self, "TerritoryPlan")
var idle_plan = IdlePlan.new(self, "IdlePlan")

func _ready() -> void:
	super()


	# TODO on start, maybe remember this fish's last plan?
	switch_plan_to(territory_plan)
	#print("At coralFish ready:", plan.behaviour)

### ------------ BEHAV LOGIC to change intent ------------ ###

func _choose_a_plan():

	if security < 98:
		switch_plan_to(territory_plan)

	# The idle plan is kind of the default when all
	# other plans are ended.
	if energy < 5 or plan.none():
		switch_plan_to(idle_plan)

	p()


func _change_stats():
	energy -= 0.01 * int(plan != idle_plan)
	security -= 1 * int(plan != territory_plan)


func _physics_process(delta: float) -> void:
	super(delta)
	move_target(delta)


var w:float = 0
@onready var op : Vector3 = %follow.position
func move_target(delta):
	## Makes the target go up and down
	var norm : Vector3 = %follow.basis.y#.normalized()
	w += delta
	var v = Vector3.ZERO#op#(2 * sin(w)) * norm
	v.x = 6 * sin(w)
	v.z = 6 * cos(w)
	#v.y = op.y * norm
	#print(sin(w))
	#%follow.position = op + (0.5*v)
	%follow.position = op + v #0.5*v
