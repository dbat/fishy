extends RigidBody3D

## Findings
##
## To get the  fish to defy gravity a little bit:
## On water_sphere Area3D
## Space override: Replace
## Point : true
## Point unit distance : 1m
## Gravity : 9.8
## AND:
## var Fb = -1  *  mass * get_gravity() # ✔ Mostly cancels gravity


enum EMO {CONTENT, HAPPY, HUNGRY, AFRAID}
enum MOVEMENT {STILL, STRAIGHT, CIRCLING}
enum STATUS {INIT, DURING, ENDING}
var status:STATUS
var movement:MOVEMENT
var emo:EMO

@export var depth_band := Vector2(11,15)
@export var start_depth : float = 12
@export var noise : NoiseTexture2D
@export var min_speed : float = 0
@export var max_speed : float = 0

@onready var water_sphere: Area3D = %water_sphere

var data:PackedByteArray
var speed:float
var fish_sphere:SphereShape3D
var q:PhysicsShapeQueryParameters3D

func _ready() -> void:
	if noise:
		await noise.changed
		var img : Image = noise.get_image()
		data = img.get_data()

	# Start fish off going forwards. I guess.
	movement = MOVEMENT.STILL
	emo = EMO.HAPPY
	status = STATUS.INIT
	#var global_fwd : Vector3 = -global_basis.z# * Vector3.FORWARD
	#apply_impulse(speed * global_fwd)
	speed = min_speed

	angular_damp = max_speed * mass * 100
	linear_damp = max_speed * mass * 10

	fish_sphere = SphereShape3D.new()
	fish_sphere.radius = 0.5

	q = PhysicsShapeQueryParameters3D.new()
	q.exclude.append(%s_core.get_rid())
	q.collide_with_bodies = true
	q.collision_mask = 2
	q.shape = %fish_sense.shape #fish_sphere
	q.margin = 0.02
	#q.transform = %fish_sense.global_transform
	#q.transform = global_transform
	#q.transform.origin = Vector3(0,0,-1)

var boost := false
## For now space will boost the fish forwards
func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.keycode == KEY_SPACE:
			boost = true

#var i:int = 0
## Right now this uses a 1d noise texture to get some
## numbers varying from -n.0 to +n.0 (whatever n is)
## to give the fish some visual up and down floatiness
#func get_buoyancy(state)->Vector3:
	#var n : float = 0
	#if noise:
		#n = data.decode_s8(i) # yay has negative numbers!
		#i = wrapi(i + 1, 0, data.size())
		##n = remap(n, 0, 255, 0, mass/100.)
		#print(n)
#
	#var fish_total_volume:float = fish_volume + n
	#var water_vol_displaced :float = fish_total_volume # archimedes
	#var water_weight :float = water_density * water_vol_displaced
	#var Fb = -1 * water_weight * state.total_gravity#.normalized()
#
	#return Fb


func nose_up(r:float):
	var T = basis.x.normalized()
	T = (T * r/2 ).normalized()
	apply_torque_impulse(-T)

func nose_down(r:float):
	var T = basis.x.normalized()
	T = (T * r/2 ).normalized()
	apply_torque_impulse(T)

func nose_left():
	var T = get_gravity().normalized() #basis.y.normalized()
	T = (T / 1000).normalized()
	apply_torque_impulse(T)

func nose_right():pass

func go_forward():
	speed = linear_velocity.length()
	#print(speed)
	if speed > max_speed: return
	if emo == EMO.AFRAID: speed = max_speed
	var fwd = speed * -global_basis.z
	apply_impulse(fwd)
	#print("go fwd:", fwd)

func stop():pass

var i:int=0
var _nlevel := true
func up_or_down():
	await get_tree().create_timer(3.0).timeout
	var n : float = 0
	if noise:
		n = data.decode_u8(i)
		i = wrapi(i + 1, 0, data.size())
		n = remap(n, 0, 255, 0, TAU)

	_nlevel = false
	if n < TAU/3.0 :
		nose_down(n)
	elif n > PI:
		nose_up(n)
	else:
		_nlevel = true


func decide_what_to_do():
	await get_tree().create_timer(4.0).timeout

	# make a decision
	emo = EMO.AFRAID

	# Decision init
	if emo == EMO.AFRAID:
		movement = MOVEMENT.CIRCLING
		status = STATUS.INIT

	if emo == EMO.CONTENT:
		movement = MOVEMENT.STRAIGHT
		status = STATUS.INIT

	# Decision init 2
	if status == STATUS.INIT:
		if movement == MOVEMENT.CIRCLING:
			nose_left()
			go_forward()

		if movement == MOVEMENT.STRAIGHT:
			go_forward()


		status = STATUS.DURING

func _process(delta: float) -> void:
	decide_what_to_do()


func _physics_process(delta: float) -> void:
	if status == STATUS.DURING:
		if movement != MOVEMENT.STILL:
			up_or_down()
		if movement == MOVEMENT.CIRCLING:
			nose_left()
			go_forward()
			_cohere(delta)
		if movement == MOVEMENT.STRAIGHT:
			go_forward()


func _nose_level(state):
	var xform : Transform3D = global_transform
	xform.basis.y = -state.total_gravity
	xform.basis.x = -xform.basis.z.cross(-state.total_gravity)
	xform.basis = xform.basis.orthonormalized()
	#global_transform = xform
	global_transform = global_transform.interpolate_with(xform, speed * state.step)


var nf:Array
func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	if _nlevel: _nose_level(state)
	_get_nearby_fish(state)


func _get_nearby_fish(state:PhysicsDirectBodyState3D):
	q.transform = global_transform
	var ss := state.get_space_state()
	nf = ss.intersect_shape(q, 2)


func _cohere(delta:float):
	for f in nf:
		var other_fish = f.collider
		#global_transform = other_fish.global_transform
		global_transform = global_transform.interpolate_with(other_fish.global_transform, speed * delta)

#var xform = align_with_y(global_transform, -state.total_gravity)
#func align_with_y(xform, new_y):
	#xform.basis.y = new_y
	#xform.basis.x = -xform.basis.z.cross(new_y)
	#xform.basis = xform.basis.orthonormalized()
	#return xform

#var crash_damping:=false
#func xx_on_body_entered(body: Node) -> void:
	#if body == %s_core: return # ignore the planet itself
	##print(self, " has hit ", body)
	#crash_damping = true
	#body.crash_damping = true
