extends RigidBody3D

## Findings
##
## To get the fish to defy gravity a little bit:
## On water_sphere Area3D
## Space override: Replace
## Point : true
## Point unit distance : 1m
## Gravity : 9.8
## AND:
## var Fb = -1  *  mass * get_gravity() # ✔ Mostly cancels gravity

class Intent extends RefCounted:
	enum {REST, PATROL, FINDFOOD, FLEE} # task
	enum {STATUS_INIT, STATUS_DURING, STATUS_ENDING} # status
	var task:int
	var status:int
	var funcmap := {}

	func _init(fish) -> void:
		funcmap = {
			REST : fish.rest,
			PATROL : fish.patrol,
			FINDFOOD : fish.find_food,
			FLEE : fish.flee
		}

	func _get_task_func() -> Callable:
		return funcmap[task]

	func call_task_func(delta, frames):
		var _task := _get_task_func()
		if _task.is_valid():
			_task.call(delta, frames)

	func _switch(_task) -> void:
		task = _task
		status = STATUS_INIT

	func to_rest(): _switch(REST)
	func to_patrol(): _switch(PATROL)
	func to_find_food(): _switch(FINDFOOD)
	func to_flee(): _switch(FLEE)

	func resting() -> bool: return task == REST and status == STATUS_DURING
	func patrolling() -> bool: return task == PATROL and status == STATUS_DURING
	func hunting() -> bool: return task == FINDFOOD and status == STATUS_DURING
	func fleeing() -> bool: return task == FLEE and status == STATUS_DURING

	func status_is_init() -> bool : return status == STATUS_INIT
	func status_is_during() -> bool : return status == STATUS_DURING
	func status_is_ending() -> bool : return status == STATUS_ENDING

	func status_to_during(): status = STATUS_DURING
	func status_to_ending(): status = STATUS_ENDING

	func _to_string() -> String:
		return "Intent: %s task: %s status:%s" % [self.get_instance_id(), task, status]


var intent := Intent.new(self)

@export var core_radius : float = 10
@export var depth_band := Vector2(11,15)
@export var start_depth : float = 12
@export var noise : NoiseTexture2D
@export var min_speed : float = 0
@export var max_speed : float = 0
@export var ALARM := false
@export var hunger := 100.0
@export var security := 10.0
@export var energy := 100.0

@onready var water_sphere: Area3D = %water_sphere

var data:PackedByteArray
var speed:float
var q:PhysicsShapeQueryParameters3D

var dead := false
var my_patrol_target : Vector3
var my_patrol_target_depth : float = start_depth


func _ready() -> void:
	if noise:
		await noise.changed
		var img : Image = noise.get_image()
		data = img.get_data()

	my_patrol_target = %patrol1.global_position

	# Start fish off chilling
	intent.to_rest()

	speed = min_speed

	angular_damp = 0
	linear_damp = 0

	## Code for the fish's eyes
	q = PhysicsShapeQueryParameters3D.new()
	#q.exclude.append(self.get_rid()) # is not working...
	q.exclude = [self.get_rid()] # wierd, but this works!
	q.collide_with_bodies = true
	q.collision_mask = 2
	# NOTE:
	# %eyes is a CollisionShape3D and it is DISABLED!
	# This means it does NOT participate in collisions.
	# I can use it to scan for intersections with the
	# PhysicsServer stuff! Yay!
	q.shape = %eyes.shape
	q.margin = 0#.02


var scan_num := 8
func _get_nearby_fish(state:PhysicsDirectBodyState3D):
	#q.transform = %senses/ahead.global_transform
	q.transform = %eyes.global_transform
	var ss := state.get_space_state()
	nf = ss.intersect_shape(q, scan_num)
	#print(nf)
	if nf:
		var seen := nf.size()
		#print(nf.size())
		#print( self, " hits ", nf[0].collider)
		#if scan_num < f.size(): scan_num = f.size()
		if seen < scan_num:
			scan_num = wrapi(seen, 2, 8)


func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.keycode == KEY_SPACE:
			pass


var testang : float
func nose_up_down(rads_per_secs:float, delta):
	## basis.get_euler().z seems to flip (slowly) between 0 and -PI
	## When basis.get_euler().z is approx equal to -PI, the fish
	## is mostly upside down.
	## BUT - Not always :(
	## Okay, basis.z.z seems to be negative when upside down.
	## BUT - Not always :(

	## Try to decide between up and down
	## Okay, going to try signed_angle_to
	## vec from marker to fish
	var fish_to_patrol:Vector3 = %patrol1.position - position
	## vector of left right yaw
	var n := -basis.z
	## Look at those two along the X axis
	var sa := fish_to_patrol.signed_angle_to(n,basis.x)
	print(sa)
	var T := basis.x
	#T = rads_per_secs * T * sign(sa)
	T = T * -sa
	angular_damp = 30
	apply_torque_impulse(T)


func nose_left_right(rads_per_secs:float, delta):
	## Try to decide between left/right
	## Okay, going to try signed_angle_to
	## vec from marker to fish
	var fish_to_patrol:Vector3 = %patrol1.global_position - global_position
	#var fish_to_patrol:Vector3 = %patrol1.position - position # this also works. Why??
	## vector of up/down pitch
	var n := -basis.z
	## Look at those two along the Y axis
	var sa := fish_to_patrol.signed_angle_to(n, basis.x)
	var T = basis.y
	T = T * -sa
	# a bigger angle should be faster - hence lower damp in proportion
	# somehow...
	angular_damp = 30
	apply_torque_impulse(T)
	#print("turning r:", r)


func swim(tspeed, delta):
	#speed = linear_velocity.length() + min_speed
	speed = min_speed+1
	#if speed > max_speed: return
	#if intent.fleeing(): speed = max_speed
	var fwd = speed * -global_basis.z
	linear_damp = 10#angular_damp/3
	#print(speed)
	apply_impulse(fwd)


func _process(delta: float) -> void:
	var frames := Engine.get_process_frames()

	if frames % 2 == 0: # every 2 frames
		hunger -= 1
		energy -= 0.2
		security -= 0.5

	if frames % 4 == 0:
		decide_what_to_do()

	move_target(delta)

func decide_what_to_do():
	if ALARM:
		intent.to_flee()
	else:
		if security < 20:
			if not intent.patrolling():
				intent.to_patrol()


func rest(delta, frames):
	pass#print("resting")
func flee(delta, frames):pass
func find_food(delta, frames):pass


var last_av:Vector3
var thrusting_up : bool = false
func patrol(delta, frames):
	var must_swim:bool = true
	if intent.status_is_init():
		intent.status_to_during()

	if intent.status_is_during():
		var tspeed : float = 5
		#var target : Vector3 = (%patrol1.position * transform) - position
		var target : Vector3 = %patrol1.position - position
		%test.position = target
		var dist := target.length()
		#print("dist:", dist)
		if dist > 0.5:
			#var rads := target.normalized().dot(linear_velocity.normalized())
			var _dot := target.normalized().dot(basis.z.normalized())
			#print("dot:", _dot)
			# _dot (-0.5 to -1.0) means TOWARDS target
			#if _dot > -0.5:
				#if false:
					## Trying to figure out whether the fish is left or right
					## of the target.
					## With help from @kroltan@functional.cafe
					##The general procedure is
					##
					## Make sure your forward and target vectors are on the same space (local, global, what have you) so the math makes sense;
					## Cross the target forward with the up vector to get the orthogonal vector "right".
					## If your target and up vectors are not orthogonal, (for example target point slightly up or down) then you need to normalise the right vector;
					## Dot product of right with fwd. Float in -1 to 1 range where -1 is left and 1 is right.
					##
					##Steps 2 and 3 might not be necessary if the target vector comes from a node, you can use its right vector instead of calculating it yourself.
					## left or right code -
					## <-- 0.n ..0.00n.. -0.n -->
					#var up := basis.y.normalized()
					#var right := target.normalized().cross(up).normalized()
					##var right := up.cross(target.normalized()).normalized()
					#print("right dot fwd:", right.dot(basis.z.normalized()))
				## _dot (-0.5 to -1.0) means TOWARDS target
				#var tspeed : float = remap(_dot, -1.0, 1.0, 1, 0.1)
				#var damp : float = remap(_dot, -1.0, 1.0, 10, 3)
				#angular_damp = damp

			# _dot -0.5 to +1 means AWAY
			# -1.0 to -0.5 means TOWARDS
			#if _dot > -0.5:
			#var ftps = Engine.get_physics_ticks_per_second()
			#print("ftps:", ftps)
			#tspeed = 			 remap(_dot, -1.0, -0.5,  PI/45, PI/90)
			#var damp : float = remap(_dot, -1.0, -0.5, 20, 5)
			#damp = clampf(damp, 1, damp)
			#tspeed = remap(_dot, -1.0, -0.5, 1, PI/90)
			#tspeed = remap(_dot, -0.5, 1.0, PI/90, PI)

			tspeed = remap(_dot, -1.0, 1.0, 1, PI)

			angular_damp = 1 #30 - (_dot * 30) + 1

			nose_up_down(tspeed, delta)
			nose_left_right(tspeed, delta)


		swim(tspeed, delta)

var slowmo := 1
func _physics_process(delta: float) -> void:
	var frames := Engine.get_physics_frames()
	if frames % slowmo == 0:
		intent.call_task_func(delta, 1)


var nf:Array
func xx_integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	var frames := Engine.get_physics_frames()
	if frames % slowmo == 0:
		var old_v3 := basis.get_euler()
		var new_v3 = old_v3
		new_v3.z = 0
		new_v3 = lerp(old_v3, new_v3, state.step)
		set_rotation(new_v3)

	#_get_nearby_fish(state)


func _nose_level(state):
	## This forces the fish to orient to gravity - fin up
	## BUT it also prevents the fish from swimming up!
	#var xform : Transform3D = global_transform
	#xform.basis.y = -state.total_gravity
	#xform.basis.x = -xform.basis.z.cross(-state.total_gravity)
	#xform.basis = xform.basis.orthonormalized()
	#global_transform = global_transform.interpolate_with(xform, speed * state.step)

	## Trying other ideas:
	#var ang_x := basis.get_euler().x
	var xform : Transform3D = transform
	xform.basis.z = xform.basis.x.cross(-state.total_gravity.normalized())
	xform.basis = xform.basis.orthonormalized()
	#xform.rotated(basis.x, ang_x)
	transform = xform#transform.interpolate_with(xform, speed * 5 * state.step)






## --- PDI Controller --

var _prev_error: float = 0.0
var _integral: float = 0.0
var _int_max = 200

# these are gains
@export var _Kp: float = 1.0 # 1.0 follows but oscillates
@export var _Ki: float = 1.0 # steady state error fix.
@export var _Kd: float = 0.1 # When 1.0 is smoother but lags, so drop to 0.1

#var _dt = 0.01
func calculate(setpoint, pv, _dt):
	var error = setpoint - pv
	var Pout = _Kp * error

	_integral += error * _dt
	var Iout = _Ki * _integral

	var derivative = (error - _prev_error) / _dt
	var Dout = _Kd * derivative

	var output = Pout + Iout + Dout

	if _integral > _int_max:
		_integral = _int_max
	elif _integral < -_int_max:
		_integral = -_int_max
	_prev_error = error
	return output


var w:float = 0
func move_target(delta):
	## Makes the target go up and down
	var norm : Vector3 = %patrol1.basis.y#.normalized()
	#var _d : float = depth_band[1]-depth_band[0]
	#my_patrol_target_depth += delta
	## this wrap is to prevent the var getting crazy-big.
	#my_patrol_target_depth = wrapf(my_patrol_target_depth,
			#depth_band[0], (depth_band[1]*2) - depth_band[0])
	#var _pp:float = pingpong(my_patrol_target_depth, _d)
	#my_patrol_target = norm * _pp
	#%patrol1.position = my_patrol_target

	w += delta * 0.2
	%patrol1.global_position = my_patrol_target + (2* sin(w)) * norm


## Trash pile:



## Mysterious voodoo from the docs
## func get_ball_inertia():
##  return PhysicsServer3D.body_get_direct_state(ball.get_rid()).inverse_inertia.inverse()
## also
## Basis get_inverse_inertia_tensor() const
## Returns the inverse inertia tensor basis. This is used to calculate the angular acceleration resulting from a torque applied to the RigidBody3D.


#var i:int = 0
## Right now this uses a 1d noise texture to get some
## numbers varying from -n.0 to +n.0 (whatever n is)
## to give the fish some visual up and down floatiness
#func get_buoyancy(state)->Vector3:
	#var n : float = 0
	#if noise:
		#n = data.decode_s8(i) # yay has negative numbers!
		#i = wrapi(i + 1, 0, data.size())
		##n = remap(n, 0, 255, 0, mass/100.)
		#print(n)
#
	#var fish_total_volume:float = fish_volume + n
	#var water_vol_displaced :float = fish_total_volume # archimedes
	#var water_weight :float = water_density * water_vol_displaced
	#var Fb = -1 * water_weight * state.total_gravity#.normalized()
#
	#return Fb


#func _cohere(delta:float):
	#for f in nf:
		#var other_fish = f.collider
		##global_transform = other_fish.global_transform
		#global_transform = global_transform.interpolate_with(other_fish.global_transform, speed * delta)

#var crash_damping:=false
#func xx_on_body_entered(body: Node) -> void:
	#if body == %s_core: return # ignore the planet itself
	##print(self, " has hit ", body)
	#crash_damping = true
	#body.crash_damping = true

#var i:int=0
#func decide_up_or_down() -> Vertical:
	#var V := Vertical.new()
	#var n : float = 0
	#if noise:
		#n = data.decode_u8(i)
		#i = wrapi(i + 1, 0, data.size())
		#n = remap(n, 0, 255, 0, TAU)
#
	#V.dir = V.LEVEL
	#V.r = 0
	#if n < TAU/3.0 :
		#V.dir = V.DOWN
		#V.r = n
	#elif n > PI:
		#V.dir = V.UP
		#V.r = n
	#return V
